/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  content: ["*.html", "./src/**/*.rs", "style/main.scss"],
  theme: {
    extend: {
      spacing: {
        '128': '32rem',
      },
      textColor: {
        'b-blue': "#4977EA"
      }
    },
    fontFamily: {
      'sans': ["Sans", 'Roboto'],
      'display': ['Roboto'],
      'body': ['Roboto'],
    }
  },
  plugins: [require("@tailwindcss/forms")],
}

