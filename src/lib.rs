use cfg_if::cfg_if;

mod db;
mod msgs;

cfg_if! {
  if #[cfg(feature = "hydrate")] {
    mod app;

    use app::App;
    use wasm_bindgen::prelude::wasm_bindgen;

    #[wasm_bindgen]
    pub fn hydrate() {
      leptos::mount_to_body(App);
    }
  }
}
