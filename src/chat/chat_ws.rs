use leptos::*;

// Entry point for our websocket route
#[cfg(feature = "ssr")]
#[get("/ws/icons/{id}")]
async fn chat_route_icon(
    req: HttpRequest,
    stream: web::Payload,
    path: web::Path<usize>,
    srv: web::Data<Addr<chat_sse::server::IconWs>>,
) -> Result<HttpResponse, Error> {
    ws::start(
        chat_sse::session::WsChatSessionIcon {
            id: *path,
            hb: std::time::Instant::now(),
            addr: srv.get_ref().clone(),
        },
        &req,
        stream,
    )
}

// Entry point for our websocket route
#[cfg(feature = "ssr")]
#[get("/ws/{id}")]
async fn chat_route(
    req: HttpRequest,
    stream: web::Payload,
    path: web::Path<usize>,
    srv: web::Data<Addr<chat_sse::server::ChatServer>>,
) -> Result<HttpResponse, Error> {
    ws::start(
        chat_sse::session::WsChatSession {
            id: 1,
            hb: std::time::Instant::now(),
            room: *path,
            name: None,
            addr: srv.get_ref().clone(),
        },
        &req,
        stream,
    )
}
