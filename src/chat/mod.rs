use actix::Addr;
use actix::*;
use actix_web::web;
use actix_web::{get, App, Error, HttpRequest, HttpResponse, HttpServer, Responder};
use actix_web_actors::ws;
use diesel::connection::BoxableConnection;
use std::ops::{Deref, DerefMut};

use diesel::prelude::*;
use dotenv::dotenv;

mod app;
use app::pages::home::HomePage;
use app::App;

use crate::db::user::{delete_user_by_id, insert_new_user};
use leptos::*;
use leptos_meta::*;
use leptos_router::*;

pub mod chat_sse;

#[cfg(feature = "ssr")]
fn render_images(folder: &str, path: actix_web::web::Path<String>) -> Vec<u8> {
  use std::io::Read;

  let path = std::env::current_dir()
        .unwrap()
        .join(folder.to_string() + "/" + &path);

  let mut file = std::fs::File::open(path).unwrap();
  let mut buffer = Vec::new();
  file.read_to_end(&mut buffer).unwrap();
  buffer
}

#[cfg(feature = "ssr")]
#[get("/upload/{image_path}")]
async fn image_path(path: actix_web::web::Path<String>, req: HttpRequest) -> HttpResponse {
  let buffer = render_images("upload", path);
  HttpResponse::Ok().body(buffer).respond_to(&req)
}

#[cfg(feature = "ssr")]
#[get("/images/{image_path}")]
async fn upload_path(path: actix_web::web::Path<String>, req: HttpRequest) -> HttpResponse {
  let buffer = render_images("images", path);
  HttpResponse::Ok().body(buffer).respond_to(&req)
}

// Entry point for our websocket route
#[cfg(feature = "ssr")]
#[get("/ws/icons/{id}")]
async fn chat_route_icon(
  req: HttpRequest,
  stream: web::Payload,
  path: web::Path<usize>,
  srv: web::Data<Addr<chat_sse::server::IconWs>>,
  ) -> Result<HttpResponse, Error> {
  ws::start(
    chat_sse::session::WsChatSessionIcon {
      id: *path,
      hb: std::time::Instant::now(),
      addr: srv.get_ref().clone(),
    },
        &req,
        stream,
    )
}

// Entry point for our websocket route
#[cfg(feature = "ssr")]
#[get("/ws/{id}")]
async fn chat_route(
  req: HttpRequest,
  stream: web::Payload,
  path: web::Path<usize>,
  srv: web::Data<Addr<chat_sse::server::ChatServer>>,
  ) -> Result<HttpResponse, Error> {
  ws::start(
    chat_sse::session::WsChatSession {
      id: 1,
      hb: std::time::Instant::now(),
      room: *path,
      name: None,
      addr: srv.get_ref().clone(),
    },
        &req,
        stream,
    )
}