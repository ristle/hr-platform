use leptos::*;

use async_broadcast::{Receiver, Sender};
use futures_util::stream::{SplitSink, SplitStream};
use futures_util::{select, FutureExt, SinkExt, StreamExt};
use gloo_net;
use gloo_net::websocket::futures::WebSocket;
use lazy_static::lazy_static;
use leptos::logging::log;
use parking_lot::{RwLockReadGuard, RwLockWriteGuard};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, sync::Arc};
use uuid::Uuid;

#[derive(Debug, Clone, PartialEq)]
pub enum StreamData {
    Message(Message),
    Close,
}

#[derive(Debug, Clone)]
pub enum SyncChannel {
    BroadCast(Sender<StreamData>, Receiver<StreamData>),
    Mpsc(Sender<StreamData>, Receiver<StreamData>),
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub enum WsData {
    MessageData,
}

#[derive(Debug)]
pub enum WebSocketState {
    NewConnection,
    PassThrough,
}

impl StreamData {
    pub fn into_inner(self) -> serde_json::Value {
        match self {
            Self::Message(message) => serde_json::to_value(message).unwrap(),
            Self::Close => serde_json::to_value("command: close").unwrap(),
        }
    }
}
pub trait ToStreamData {
    fn from_inner(inner: &str) -> Result<StreamData, std::io::Error>;
}

impl ToStreamData for String {
    fn from_inner(inner: &str) -> Result<StreamData, std::io::Error> {
        let value: serde_json::Value = serde_json::from_str(inner.trim())?;
        if let Ok(message) = serde_json::from_value::<Message>(value.clone()) {
            return Ok(StreamData::Message(message));
        }
        {
            log!("Error with stream text: {}", inner);
            Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                "Invalid Data",
            ))
        }
    }
}

impl SyncChannel {
    pub async fn send(&mut self, message: StreamData) {
        match self {
            SyncChannel::BroadCast(tx, _) => {
                let _ = tx.broadcast(message).await.unwrap();
            }
            SyncChannel::Mpsc(tx, _) => {
                tx.broadcast(message)
                    .await
                    .expect("Failed to Send Message to Other Threads");
            }
        }
    }

    pub async fn next(&mut self) -> Option<StreamData> {
        match self {
            SyncChannel::BroadCast(_, ref mut rx) => rx.next().await,
            SyncChannel::Mpsc(_, ref mut rx) => rx.next().await,
        }
    }

    pub async fn rebound_stream<E, T: 'static>(
        &mut self,
        messages: impl Fn() -> Option<RwSignal<T>>,
        function: impl Fn(Option<&mut T>, E) + 'static,
    ) where
        E: for<'de> Deserialize<'de> + std::any::Any + std::fmt::Debug, // Add this line
    {
        while let Some(data) = self.next().await {
            let mut value: E;
            value = serde_json::from_value(data.into_inner()).unwrap();
            value = *Box::<dyn std::any::Any>::downcast::<E>(Box::new(value)).unwrap();
            match messages() {
                Some(messages) => messages.update(|signal_inner| {
                    function(Some(signal_inner), value);
                }),
                None => function(None, value),
            }
        }
    }
}

pub type WsVecType = Arc<parking_lot::RwLock<HashMap<(WsData, i32), SyncChannel>>>;

lazy_static! {
    #[derive(Debug)]
    pub static ref STREAMVEC: WsVecType = Arc::new(parking_lot::RwLock::new(HashMap::new()));
}

lazy_static! {
    #[derive(Debug)]
    pub static ref SINKVEC: WsVecType = Arc::new(parking_lot::RwLock::new(HashMap::new()));
}

fn push_to_map<'a, F>(accessor: F, id: i32, data: WsData, channel: SyncChannel)
where
    F: FnOnce() -> RwLockWriteGuard<'a, HashMap<(WsData, i32), SyncChannel>>,
{
    let mut glob_vec = accessor();
    glob_vec.insert((data, id), channel);
}

fn retrieve_from_map<'a, F>(accessor: F, data: WsData, id: i32) -> Option<SyncChannel>
where
    F: FnOnce() -> RwLockReadGuard<'a, HashMap<(WsData, i32), SyncChannel>>,
{
    let glob_vec = accessor();
    glob_vec.get(&(data, id)).cloned()
}

impl STREAMVEC {
    pub fn sync_stream(id: i32, data: WsData) -> (SyncChannel, WebSocketState) {
        match retrieve_from_map(|| STREAMVEC.read(), data, id) {
            Some(dual_channel) => {
                let channel = match dual_channel {
                    SyncChannel::BroadCast(tx, rx) => SyncChannel::BroadCast(tx, rx.new_receiver()),
                    SyncChannel::Mpsc(..) => dual_channel,
                };
                (channel, WebSocketState::PassThrough)
            }
            None => {
                let channel = match data {
                    WsData::MessageData => {
                        let (tx, rx) = async_broadcast::broadcast::<StreamData>(100000);
                        SyncChannel::Mpsc(tx, rx)
                    }
                };
                push_to_map(|| STREAMVEC.write(), id, data, channel.clone());
                (channel, WebSocketState::NewConnection)
            }
        }
    }
}

impl SINKVEC {
    pub fn sync_stream(id: i32, data: WsData) -> SyncChannel {
        match retrieve_from_map(|| SINKVEC.read(), data, id) {
            Some(dual_channel) => match dual_channel {
                SyncChannel::BroadCast(tx, rx) => SyncChannel::BroadCast(tx, rx.new_receiver()),
                SyncChannel::Mpsc(tx, rx) => SyncChannel::Mpsc(tx, rx),
            },
            None => {
                let channel = match data {
                    WsData::MessageData => {
                        let (tx, rx) = async_broadcast::broadcast::<StreamData>(100000);
                        SyncChannel::Mpsc(tx, rx)
                    }
                };
                push_to_map(|| SINKVEC.write(), id, data, channel.clone());
                channel
            }
        }
    }

    pub fn send_clear() {
        spawn_local(async move {
            let mut lock = SINKVEC.write();
            for (_, sync_channel) in lock.iter_mut() {
                sync_channel.send(StreamData::Close).await
            }
            lock.clear();
        })
    }
}

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct Count {
    pub value: i32,
}
#[derive(Debug, serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub struct Message {
    pub message: Option<String>,
    pub image: Option<String>,
    pub conversation_id: i32,
    pub user_id: i32,
    pub first_name: String,
    pub last_name: String,
    pub seen: Option<Vec<(String, String)>>,
    pub message_id: i32,
}

pub struct HandleWebSocket;

impl HandleWebSocket {
    fn handle_websocket(
        url: &str,
        id: i32,
    ) -> (
        SplitSink<WebSocket, gloo_net::websocket::Message>,
        SplitStream<WebSocket>,
    ) {
        let data = format!("{url}{id}").clone();
        dbg!(data);
        let ws = WebSocket::open(&format!("{url}{id}")).unwrap();
        ws.split()
    }

    pub async fn handle_split_stream<'a, T, E>(
        id: i32,
        messages: Option<RwSignal<T>>,
        url: &str,
        function: impl Fn(Option<&mut T>, E) + 'static + Clone,
    ) where
        E: for<'de> Deserialize<'de> + std::any::Any + std::fmt::Debug, // Add this line
        T: std::fmt::Debug,
    {
        let data = match std::any::TypeId::of::<E>() {
            _ => WsData::MessageData,
        };

        let (mut sync_channel, _state) = STREAMVEC::sync_stream(id, data);
        let _sync_clone = sync_channel.clone();
        let _function_clone = function.clone();
        let mut rx_sink = SINKVEC::sync_stream(id, data);
        let _rx_sink_clone = rx_sink.clone();
        let messages = move || messages;

        let (mut sink, mut ws_read) = Self::handle_websocket(url, id);
        spawn_local(async move {
            loop {
                select! {
                        message = rx_sink.next().fuse() => {
                            if message.as_ref().unwrap() == &StreamData::Close {
                                sink.close().await.unwrap()
                            } else {
                            sink.send(gloo_net::websocket::Message::Text(
                                serde_json::to_string(&message.unwrap().into_inner()).unwrap(),
                            ))
                            .await
                            .unwrap()
                        }
                        },
                        value = ws_read.next().fuse() => {
                            if let Some(value) = value {
                                match value {
                                Ok(gloo_net::websocket::Message::Text(text)) => {
                                    let _ = sync_channel
                                        .send(std::string::String::from_inner(text.trim()).unwrap())
                                        .await;
                                    let value: E = serde_json::from_str(&text).unwrap();
                                    dbg!(messages().clone());
                                    match messages() {
                                        Some(messages) => messages.update(|signal_inner| {
                                            function(Some(signal_inner), value);
                                        }),
                                        None => function(None, value),
                                    };
                                },
                                Ok(gloo_net::websocket::Message::Bytes(_)) => {
                                    log!("BYTES?");
                                },
                                _ => (),
                            };
                        }
                    }
                }
            }
        });
    }
}
