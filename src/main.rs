#![feature(return_position_impl_trait_in_trait)]

use actix_session::SessionMiddleware;
use actix_web::{
    cookie::{Key, SameSite},
    App, HttpResponse, HttpServer, Responder,
};
use actix_web::{get, HttpRequest};
use crate::msgs::Config;

use actix_web::http::header;
use actix_cors::Cors;

use diesel::prelude::*;
use dotenv::dotenv;

mod app;
use app::App;

use leptos::*;

mod db;
mod msgs;
mod server_functions;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

#[cfg(feature = "ssr")]
fn render_images(folder: &str, path: actix_web::web::Path<String>) -> Vec<u8> {
    use std::io::Read;

    let path = std::env::current_dir()
        .unwrap()
        .join(folder.to_string() + "/" + &path);

    let mut file = std::fs::File::open(path).unwrap();
    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer).unwrap();
    buffer
}

#[cfg(feature = "ssr")]
#[get("/upload/{image_path}")]
async fn image_path(path: actix_web::web::Path<String>, req: HttpRequest) -> HttpResponse {
    let buffer = render_images("upload", path);
    HttpResponse::Ok().body(buffer).respond_to(&req)
}

#[cfg(feature = "ssr")]
#[get("/images/{image_path}")]
async fn upload_path(path: actix_web::web::Path<String>, req: HttpRequest) -> HttpResponse {
    let buffer = render_images("images", path);
    HttpResponse::Ok().body(buffer).respond_to(&req)
}

#[cfg(feature = "ssr")]
fn session_middleware() -> SessionMiddleware<actix_session::storage::CookieSessionStore> {
    SessionMiddleware::builder(
        actix_session::storage::CookieSessionStore::default(),
        Key::from(&[0; 64]),
    )
    .cookie_http_only(false)
    // allow the cookie only from the current domain
    .cookie_same_site(SameSite::Strict)
    .build()
}

#[cfg(feature = "ssr")]
#[actix_web::main]
async fn main() {
    use leptos::*;

    use actix_files::Files;
    use actix_identity::IdentityMiddleware;
    use actix_web::middleware::{Compress, Logger, NormalizePath};

    env_logger::init_from_env(env_logger::Env::new().default_filter_or("warn"));

    use leptos_actix::{generate_route_list, LeptosRoutes};

    let conf = get_configuration(None).await.unwrap();
    let addr = conf.leptos_options.site_addr;

    let _leptos_options = &conf.leptos_options;

    let db_connection = establish_connection();
    let server_data = actix_web::web::Data::new(tokio::sync::Mutex::new(crate::msgs::ServerData {
        connection: db_connection,
        env: Config::init(),
        user_data: std::collections::HashMap::new(),
    }));

    HttpServer::new(move || {
        let leptos_options = &conf.leptos_options;
        let site_root = &leptos_options.site_root;
        let routes = generate_route_list(|| view! { <App/> });

        let cors = Cors::default()
            .allowed_origin("http://localhost:3000")
            .allowed_methods(vec!["GET", "POST"])
            .allowed_headers(vec![
                header::CONTENT_TYPE,
                header::AUTHORIZATION,
                header::ACCEPT,
            ])
            .supports_credentials();

        App::new()
            .app_data(server_data.clone())
            .app_data(actix_web::web::PayloadConfig::new(10_485_760))
            .wrap(IdentityMiddleware::default())
            .wrap(session_middleware())
            .route("/api/{tail:.*}", leptos_actix::handle_server_fns())
            .route("/api/user/{tail:.*}", leptos_actix::handle_server_fns())
            .leptos_routes(
                leptos_options.to_owned(),
                routes.to_owned(),
                || view! {  <App/> },
            )
            .wrap(Logger::new("%r %U").log_target("actix"))
            .wrap(Compress::default())
            .wrap(cors)
            .wrap(NormalizePath::new(
                actix_web::middleware::TrailingSlash::Trim,
            ))
            .service(Files::new("/", site_root))
    })
    .bind(&addr)
    .unwrap()
    .run()
    .await
    .unwrap();
}

#[cfg(not(feature = "ssr"))]
pub fn main() {
    leptos::mount_to_body(App);
}
