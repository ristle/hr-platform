#[cfg(feature = "ssr")]
use crate::msgs::user::{NewPlatformUser, PlatformUser};
#[cfg(feature = "ssr")]
use chrono;
#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
pub fn insert_new_user(
    conn: &mut PgConnection,
    name: String,
    surname: String,
    email: String,
    password: String,
    role: String,
    photo_url: String,
) -> PlatformUser {
    use crate::db::schema::platform_user;

    let name = name.clone();
    let email = email.clone();
    let role = role.clone();
    let photo_url = photo_url.clone();

    let new_platform_user = NewPlatformUser {
        name: name.as_str(),
        surname: surname.as_str(),
        email: email.as_str(),
        role: role.as_str(),
        password: password.as_str(),
        photo_url: Some(photo_url.as_str()),
        verified: false,
        created_at: chrono::Utc::now().naive_utc().date(),
        updated_at: chrono::Utc::now().naive_utc().date(),
    };

    dbg!(new_platform_user.clone());

    diesel::insert_into(platform_user::table)
        .values(&new_platform_user)
        .returning(PlatformUser::as_returning())
        .get_result(conn)
        .expect("Error saving new new platform user")
}

#[cfg(feature = "ssr")]
pub fn get_platform_user(conn: &mut PgConnection) -> Vec<PlatformUser> {
    use crate::db::schema::platform_user::dsl::platform_user;

    platform_user.load::<PlatformUser>(conn).unwrap().to_vec()
}

#[cfg(feature = "ssr")]
pub fn _find_new_user(conn: &mut PgConnection, id: i32) -> Option<PlatformUser> {
    use crate::db::schema::platform_user::dsl::platform_user;

    let data = platform_user
        .find(id)
        .select(PlatformUser::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn find_new_user_by_email(conn: &mut PgConnection, _email: String) -> Option<PlatformUser> {
    use crate::db::schema::platform_user::dsl::{email, platform_user};

    let data = platform_user
        .filter(email.eq(_email))
        .select(PlatformUser::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}
#[cfg(feature = "ssr")]
pub fn _update_name_user(
    conn: &mut PgConnection,
    id: i32,
    name: String,
) -> Result<PlatformUser, diesel::result::Error> {
    use crate::db::schema::platform_user::dsl::{name, platform_user};

    diesel::update(platform_user.find(id))
        .set(name.eq(name))
        .returning(PlatformUser::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_email_user(
    conn: &mut PgConnection,
    id: i32,
    email: String,
) -> Result<PlatformUser, diesel::result::Error> {
    use crate::db::schema::platform_user::dsl::{email, platform_user};

    diesel::update(platform_user.find(id))
        .set(email.eq(email))
        .returning(PlatformUser::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_role_user(
    conn: &mut PgConnection,
    id: i32,
    role: String,
) -> Result<PlatformUser, diesel::result::Error> {
    use crate::db::schema::platform_user::dsl::{platform_user, role};

    diesel::update(platform_user.find(id))
        .set(role.eq(role))
        .returning(PlatformUser::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_photo_url_platform_user(
    conn: &mut PgConnection,
    id: i32,
    _photo_url: String,
) -> Result<PlatformUser, diesel::result::Error> {
    use crate::db::schema::platform_user::dsl::{photo_url, platform_user};

    diesel::update(platform_user.find(id))
        .set(photo_url.eq(_photo_url))
        .returning(PlatformUser::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn add_verify_to_user(
    conn: &mut PgConnection,
    id: i32,
) -> Result<PlatformUser, diesel::result::Error> {
    use crate::db::schema::platform_user::dsl::{platform_user, verified};

    diesel::update(platform_user.find(id))
        .set(verified.eq(true))
        .returning(PlatformUser::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_user_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::platform_user::dsl::{id, platform_user};

    diesel::delete(platform_user.filter(id.eq(_id))).execute(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_user_by_email(
    conn: &mut PgConnection,
    email_: String,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::platform_user::dsl::{email, platform_user};

    diesel::delete(platform_user.filter(email.eq(email_))).execute(conn)
}
