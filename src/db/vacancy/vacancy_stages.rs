#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::vacancy::vacancy_stages::{NewVacancyStages, VacancyStages};

#[cfg(feature = "ssr")]
pub fn _insert_vacancy_stages(conn: &mut PgConnection, name: &'static str) -> VacancyStages {
    use crate::db::schema::vacancy_stages;

    let new_vacancy_stages = NewVacancyStages { name };

    diesel::insert_into(vacancy_stages::table)
        .values(&new_vacancy_stages)
        .returning(VacancyStages::as_returning())
        .get_result(conn)
        .expect("Error saving new vacancy_stages")
}
#[cfg(feature = "ssr")]
pub fn _find_vacancy_stages(conn: &mut PgConnection, id: i32) -> Option<VacancyStages> {
    use crate::db::schema::vacancy_stages::dsl::vacancy_stages;

    let data = vacancy_stages
        .find(id)
        .select(VacancyStages::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_name_vacancy_stages(
    conn: &mut PgConnection,
    id: i32,
    _name: &'static str,
) -> Result<VacancyStages, diesel::result::Error> {
    use crate::db::schema::vacancy_stages::dsl::{name, vacancy_stages};

    diesel::update(vacancy_stages.find(id))
        .set(name.eq(_name))
        .returning(VacancyStages::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_vacancy_stages_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::vacancy_stages::dsl::{id, vacancy_stages};

    diesel::delete(vacancy_stages.filter(id.eq(_id))).execute(conn)
}
