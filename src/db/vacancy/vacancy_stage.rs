#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::vacancy::vacancy_stage::{NewVacancyStage, VacancyStage};

#[cfg(feature = "ssr")]
pub fn _insert_vacancy_stage(
    conn: &mut PgConnection,
    vacancy_stage_id: i32,
    name: &'static str,
    is_active: bool,
) -> VacancyStage {
    use crate::db::schema::vacancy_stage;

    let new_vacancy_stage = NewVacancyStage {
        vacancy_stage_id,
        name,
        is_active,
    };

    diesel::insert_into(vacancy_stage::table)
        .values(&new_vacancy_stage)
        .returning(VacancyStage::as_returning())
        .get_result(conn)
        .expect("Error saving new vacancy_stage")
}

#[cfg(feature = "ssr")]
pub fn _find_vacancy_stage(conn: &mut PgConnection, id: i32) -> Option<VacancyStage> {
    use crate::db::schema::vacancy_stage::dsl::vacancy_stage;

    let data = vacancy_stage
        .find(id)
        .select(VacancyStage::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_vacancy_stage_id_vacancy_stage(
    conn: &mut PgConnection,
    id: i32,
    _vacancy_stage_id: i32,
) -> Result<VacancyStage, diesel::result::Error> {
    use crate::db::schema::vacancy_stage::dsl::{vacancy_stage, vacancy_stage_id};

    diesel::update(vacancy_stage.find(id))
        .set(vacancy_stage_id.eq(_vacancy_stage_id))
        .returning(VacancyStage::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_name_vacancy_stage(
    conn: &mut PgConnection,
    id: i32,
    _name: &'static str,
) -> Result<VacancyStage, diesel::result::Error> {
    use crate::db::schema::vacancy_stage::dsl::{name, vacancy_stage};

    diesel::update(vacancy_stage.find(id))
        .set(name.eq(_name))
        .returning(VacancyStage::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_is_active_vacancy_stage(
    conn: &mut PgConnection,
    id: i32,
    _is_active: bool,
) -> Result<VacancyStage, diesel::result::Error> {
    use crate::db::schema::vacancy_stage::dsl::{is_active, vacancy_stage};

    diesel::update(vacancy_stage.find(id))
        .set(is_active.eq(_is_active))
        .returning(VacancyStage::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_vacancy_stage_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::vacancy_stage::dsl::{id, vacancy_stage};

    diesel::delete(vacancy_stage.filter(id.eq(_id))).execute(conn)
}
