#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::vacancy::vacancy_people_in_work::{NewVacancyPeopleInWork, VacancyPeopleInWork};

#[cfg(feature = "ssr")]
pub fn _insert_vacancy_people_in_work(
    conn: &mut PgConnection,
    user_id: i32,
) -> VacancyPeopleInWork {
    use crate::db::schema::vacancy_people_in_work;

    let new_vacancy_people_in_work = NewVacancyPeopleInWork { user_id };

    diesel::insert_into(vacancy_people_in_work::table)
        .values(&new_vacancy_people_in_work)
        .returning(VacancyPeopleInWork::as_returning())
        .get_result(conn)
        .expect("Error saving new vacancy_people_in_work")
}
#[cfg(feature = "ssr")]
pub fn _find_vacancy_people_in_work(
    conn: &mut PgConnection,
    id: i32,
) -> Option<VacancyPeopleInWork> {
    use crate::db::schema::vacancy_people_in_work::dsl::vacancy_people_in_work;

    let data = vacancy_people_in_work
        .find(id)
        .select(VacancyPeopleInWork::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_user_id_vacancy_people_in_work(
    conn: &mut PgConnection,
    id: i32,
    _user_id: i32,
) -> Result<VacancyPeopleInWork, diesel::result::Error> {
    use crate::db::schema::vacancy_people_in_work::dsl::{user_id, vacancy_people_in_work};

    diesel::update(vacancy_people_in_work.find(id))
        .set(user_id.eq(_user_id))
        .returning(VacancyPeopleInWork::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_vacancy_people_in_work_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::vacancy_people_in_work::dsl::{id, vacancy_people_in_work};

    diesel::delete(vacancy_people_in_work.filter(id.eq(_id))).execute(conn)
}
