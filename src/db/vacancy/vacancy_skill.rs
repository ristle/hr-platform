#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::vacancy::vacancy_skill::{NewVacancySkill, VacancySkill};

#[cfg(feature = "ssr")]
pub fn _insert_vacancy_skill(
    conn: &mut PgConnection,
    user_id: i32,
    name: &'static str,
    experience_time: f64,
) -> VacancySkill {
    use crate::db::schema::vacancy_skill;

    let new_vacancy_skill = NewVacancySkill {
        user_id,
        name,
        experience_time,
    };

    diesel::insert_into(vacancy_skill::table)
        .values(&new_vacancy_skill)
        .returning(VacancySkill::as_returning())
        .get_result(conn)
        .expect("Error saving new vacancy_skill")
}

#[cfg(feature = "ssr")]
pub fn _find_vacancy_skill(conn: &mut PgConnection, id: i32) -> Option<VacancySkill> {
    use crate::db::schema::vacancy_skill::dsl::vacancy_skill;

    let data = vacancy_skill
        .find(id)
        .select(VacancySkill::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_user_id_vacancy_skill(
    conn: &mut PgConnection,
    id: i32,
    _user_id: i32,
) -> Result<VacancySkill, diesel::result::Error> {
    use crate::db::schema::vacancy_skill::dsl::{user_id, vacancy_skill};

    diesel::update(vacancy_skill.find(id))
        .set(user_id.eq(_user_id))
        .returning(VacancySkill::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_name_vacancy_skill(
    conn: &mut PgConnection,
    id: i32,
    _name: &'static str,
) -> Result<VacancySkill, diesel::result::Error> {
    use crate::db::schema::vacancy_skill::dsl::{name, vacancy_skill};

    diesel::update(vacancy_skill.find(id))
        .set(name.eq(_name))
        .returning(VacancySkill::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_experience_time_vacancy_skill(
    conn: &mut PgConnection,
    id: i32,
    _experience_time: f64,
) -> Result<VacancySkill, diesel::result::Error> {
    use crate::db::schema::vacancy_skill::dsl::{experience_time, vacancy_skill};

    diesel::update(vacancy_skill.find(id))
        .set(experience_time.eq(_experience_time))
        .returning(VacancySkill::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_vacancy_skill_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::vacancy_skill::dsl::{id, vacancy_skill};

    diesel::delete(vacancy_skill.filter(id.eq(_id))).execute(conn)
}
