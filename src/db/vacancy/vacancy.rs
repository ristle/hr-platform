#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::vacancy::vacancy::{NewVacancy, Vacancy};

#[cfg(feature = "ssr")]
pub fn _insert_vacancy(
    conn: &mut PgConnection,
    job_name: &'static str,
    company_name: &'static str,
    work_experience: f64,
    work_type: &'static str,
    feature_id: i32,
    new_responses: i32,
    unread_responses: i32,
    skills: i32,
    details: &'static str,
    vacancy_stages: i32,
    people_in_work_id: i32,
    related_people_id: i32,
) -> Vacancy {
    use crate::db::schema::vacancy;

    let new_vacancy = NewVacancy {
        job_name,
        company_name,
        work_experience,
        work_type,
        feature_id,
        new_responses,
        unread_responses,
        skills,
        details,
        vacancy_stages,
        people_in_work_id,
        related_people_id,
    };

    diesel::insert_into(vacancy::table)
        .values(&new_vacancy)
        .returning(Vacancy::as_returning())
        .get_result(conn)
        .expect("Error saving new vacancy")
}
#[cfg(feature = "ssr")]
pub fn _find_vacancy(conn: &mut PgConnection, id: i32) -> Option<Vacancy> {
    use crate::db::schema::vacancy::dsl::vacancy;

    let data = vacancy
        .find(id)
        .select(Vacancy::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_job_name_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _job_name: &'static str,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{job_name, vacancy};

    diesel::update(vacancy.find(id))
        .set(job_name.eq(_job_name))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_company_name_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _company_name: &'static str,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{company_name, vacancy};

    diesel::update(vacancy.find(id))
        .set(company_name.eq(_company_name))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_work_experience_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _work_experience: f64,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{vacancy, work_experience};

    diesel::update(vacancy.find(id))
        .set(work_experience.eq(_work_experience))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_work_type_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _work_type: &'static str,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{vacancy, work_type};

    diesel::update(vacancy.find(id))
        .set(work_type.eq(_work_type))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_feature_id_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _feature_id: i32,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{feature_id, vacancy};

    diesel::update(vacancy.find(id))
        .set(feature_id.eq(_feature_id))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_new_responses_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _new_responses: i32,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{new_responses, vacancy};

    diesel::update(vacancy.find(id))
        .set(new_responses.eq(_new_responses))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_unread_responses_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _unread_responses: i32,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{unread_responses, vacancy};

    diesel::update(vacancy.find(id))
        .set(unread_responses.eq(_unread_responses))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_skills_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _skills: i32,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{skills, vacancy};

    diesel::update(vacancy.find(id))
        .set(skills.eq(_skills))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_details_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _details: &'static str,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{details, vacancy};

    diesel::update(vacancy.find(id))
        .set(details.eq(_details))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_vacancy_stages_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _vacancy_stages: i32,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{vacancy, vacancy_stages};

    diesel::update(vacancy.find(id))
        .set(vacancy_stages.eq(_vacancy_stages))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_people_in_work_id_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _people_in_work_id: i32,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{people_in_work_id, vacancy};

    diesel::update(vacancy.find(id))
        .set(people_in_work_id.eq(_people_in_work_id))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_related_people_id_vacancy(
    conn: &mut PgConnection,
    id: i32,
    _related_people_id: i32,
) -> Result<Vacancy, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{related_people_id, vacancy};

    diesel::update(vacancy.find(id))
        .set(related_people_id.eq(_related_people_id))
        .returning(Vacancy::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_vacancy_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::vacancy::dsl::{id, vacancy};

    diesel::delete(vacancy.filter(id.eq(_id))).execute(conn)
}
