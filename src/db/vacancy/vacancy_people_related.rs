#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::vacancy::vacancy_people_related::{NewVacancyPeopleRelated, VacancyPeopleRelated};

#[cfg(feature = "ssr")]
pub fn _insert_vacancy_people_related(
    conn: &mut PgConnection,
    user_id: i32,
) -> VacancyPeopleRelated {
    use crate::db::schema::vacancy_people_related;

    let new_vacancy_people_related = NewVacancyPeopleRelated { user_id };

    diesel::insert_into(vacancy_people_related::table)
        .values(&new_vacancy_people_related)
        .returning(VacancyPeopleRelated::as_returning())
        .get_result(conn)
        .expect("Error saving new vacancy_people_related")
}
#[cfg(feature = "ssr")]
pub fn _find_vacancy_people_related(
    conn: &mut PgConnection,
    id: i32,
) -> Option<VacancyPeopleRelated> {
    use crate::db::schema::vacancy_people_related::dsl::vacancy_people_related;

    let data = vacancy_people_related
        .find(id)
        .select(VacancyPeopleRelated::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_user_id_vacancy_people_related(
    conn: &mut PgConnection,
    id: i32,
    _user_id: i32,
) -> Result<VacancyPeopleRelated, diesel::result::Error> {
    use crate::db::schema::vacancy_people_related::dsl::{user_id, vacancy_people_related};

    diesel::update(vacancy_people_related.find(id))
        .set(user_id.eq(_user_id))
        .returning(VacancyPeopleRelated::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_vacancy_people_related_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::vacancy_people_related::dsl::{id, vacancy_people_related};

    diesel::delete(vacancy_people_related.filter(id.eq(_id))).execute(conn)
}
