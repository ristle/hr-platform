#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::vacancy::vacancy_response::{NewVacancyResponse, VacancyResponse};

#[cfg(feature = "ssr")]
pub fn _insert_vacancy_response(
    conn: &mut PgConnection,
    user_id: i32,
    vacancy_id: i32,
    status: &'static str,
    vacancy_stage_id: i32,
    hr_user_id: i32,
    is_read: bool,
) -> VacancyResponse {
    use crate::db::schema::vacancy_response;

    let new_vacancy_response = NewVacancyResponse {
        user_id,
        vacancy_id,
        status,
        vacancy_stage_id,
        hr_user_id,
        is_read,
    };

    diesel::insert_into(vacancy_response::table)
        .values(&new_vacancy_response)
        .returning(VacancyResponse::as_returning())
        .get_result(conn)
        .expect("Error saving new vacancy_response")
}
#[cfg(feature = "ssr")]
pub fn _find_vacancy_response(conn: &mut PgConnection, id: i32) -> Option<VacancyResponse> {
    use crate::db::schema::vacancy_response::dsl::vacancy_response;

    let data = vacancy_response
        .find(id)
        .select(VacancyResponse::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_user_id_vacancy_response(
    conn: &mut PgConnection,
    id: i32,
    _user_id: i32,
) -> Result<VacancyResponse, diesel::result::Error> {
    use crate::db::schema::vacancy_response::dsl::{user_id, vacancy_response};

    diesel::update(vacancy_response.find(id))
        .set(user_id.eq(_user_id))
        .returning(VacancyResponse::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_vacancy_id_vacancy_response(
    conn: &mut PgConnection,
    id: i32,
    _vacancy_id: i32,
) -> Result<VacancyResponse, diesel::result::Error> {
    use crate::db::schema::vacancy_response::dsl::{vacancy_id, vacancy_response};

    diesel::update(vacancy_response.find(id))
        .set(vacancy_id.eq(_vacancy_id))
        .returning(VacancyResponse::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_status_vacancy_response(
    conn: &mut PgConnection,
    id: i32,
    _status: &'static str,
) -> Result<VacancyResponse, diesel::result::Error> {
    use crate::db::schema::vacancy_response::dsl::{status, vacancy_response};

    diesel::update(vacancy_response.find(id))
        .set(status.eq(_status))
        .returning(VacancyResponse::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_vacancy_stage_id_vacancy_response(
    conn: &mut PgConnection,
    id: i32,
    _vacancy_stage_id: i32,
) -> Result<VacancyResponse, diesel::result::Error> {
    use crate::db::schema::vacancy_response::dsl::{vacancy_response, vacancy_stage_id};

    diesel::update(vacancy_response.find(id))
        .set(vacancy_stage_id.eq(_vacancy_stage_id))
        .returning(VacancyResponse::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_hr_user_id_vacancy_response(
    conn: &mut PgConnection,
    id: i32,
    _hr_user_id: i32,
) -> Result<VacancyResponse, diesel::result::Error> {
    use crate::db::schema::vacancy_response::dsl::{hr_user_id, vacancy_response};

    diesel::update(vacancy_response.find(id))
        .set(hr_user_id.eq(_hr_user_id))
        .returning(VacancyResponse::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_is_read_vacancy_response(
    conn: &mut PgConnection,
    id: i32,
    _is_read: bool,
) -> Result<VacancyResponse, diesel::result::Error> {
    use crate::db::schema::vacancy_response::dsl::{is_read, vacancy_response};

    diesel::update(vacancy_response.find(id))
        .set(is_read.eq(_is_read))
        .returning(VacancyResponse::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_vacancy_response_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::vacancy_response::dsl::{id, vacancy_response};

    diesel::delete(vacancy_response.filter(id.eq(_id))).execute(conn)
}
