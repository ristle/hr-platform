#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::chat::chat_message_participant::{
    ChatMessageParticipant, NewChatMessageParticipant,
};

#[cfg(feature = "ssr")]
pub fn _insert_chat_message_participant(
    conn: &mut PgConnection,
    recipient_id: i32,
    recipient_group_id: i32,
    message_id: i32,
    is_read: bool,
) -> ChatMessageParticipant {
    use crate::db::schema::chat_message_participant;

    let new_chat_message_participant = NewChatMessageParticipant {
        recipient_id,
        recipient_group_id,
        message_id,
        is_read,
    };

    diesel::insert_into(chat_message_participant::table)
        .values(&new_chat_message_participant)
        .returning(ChatMessageParticipant::as_returning())
        .get_result(conn)
        .expect("Error saving new chat_message_participant")
}
#[cfg(feature = "ssr")]
pub fn _find_chat_message_participant(
    conn: &mut PgConnection,
    id: i32,
) -> Option<ChatMessageParticipant> {
    use crate::db::schema::chat_message_participant::dsl::chat_message_participant;

    let data = chat_message_participant
        .find(id)
        .select(ChatMessageParticipant::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_recipient_id_chat_message_participant(
    conn: &mut PgConnection,
    id: i32,
    _recipient_id: i32,
) -> Result<ChatMessageParticipant, diesel::result::Error> {
    use crate::db::schema::chat_message_participant::dsl::{
        chat_message_participant, recipient_id,
    };

    diesel::update(chat_message_participant.find(id))
        .set(recipient_id.eq(_recipient_id))
        .returning(ChatMessageParticipant::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_recipient_group_id_chat_message_participant(
    conn: &mut PgConnection,
    id: i32,
    _recipient_group_id: i32,
) -> Result<ChatMessageParticipant, diesel::result::Error> {
    use crate::db::schema::chat_message_participant::dsl::{
        chat_message_participant, recipient_group_id,
    };

    diesel::update(chat_message_participant.find(id))
        .set(recipient_group_id.eq(_recipient_group_id))
        .returning(ChatMessageParticipant::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_message_id_chat_message_participant(
    conn: &mut PgConnection,
    id: i32,
    _message_id: i32,
) -> Result<ChatMessageParticipant, diesel::result::Error> {
    use crate::db::schema::chat_message_participant::dsl::{chat_message_participant, message_id};

    diesel::update(chat_message_participant.find(id))
        .set(message_id.eq(_message_id))
        .returning(ChatMessageParticipant::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_is_read_chat_message_participant(
    conn: &mut PgConnection,
    id: i32,
    _is_read: bool,
) -> Result<ChatMessageParticipant, diesel::result::Error> {
    use crate::db::schema::chat_message_participant::dsl::{chat_message_participant, is_read};

    diesel::update(chat_message_participant.find(id))
        .set(is_read.eq(_is_read))
        .returning(ChatMessageParticipant::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_chat_message_participant_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::chat_message_participant::dsl::{chat_message_participant, id};

    diesel::delete(chat_message_participant.filter(id.eq(_id))).execute(conn)
}
