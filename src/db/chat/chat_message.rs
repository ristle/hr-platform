#[cfg(feature = "ssr")]
use crate::msgs::chat::chat_message::{ChatMessage, NewChatMessage};
#[cfg(feature = "ssr")]
use chrono;
#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
pub fn _insert_chat_message(
    conn: &mut PgConnection,
    subject: &'static str,
    creator_id: i32,
    message_body: &'static str,
    create_date: chrono::NaiveDate,
) -> ChatMessage {
    use crate::db::schema::chat_message;

    let new_chat_message = NewChatMessage {
        subject,
        creator_id,
        message_body,
        create_date,
    };

    diesel::insert_into(chat_message::table)
        .values(&new_chat_message)
        .returning(ChatMessage::as_returning())
        .get_result(conn)
        .expect("Error saving new chat_message")
}
#[cfg(feature = "ssr")]
pub fn _find_chat_message(conn: &mut PgConnection, id: i32) -> Option<ChatMessage> {
    use crate::db::schema::chat_message::dsl::chat_message;

    let data = chat_message
        .find(id)
        .select(ChatMessage::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_subject_chat_message(
    conn: &mut PgConnection,
    id: i32,
    _subject: &'static str,
) -> Result<ChatMessage, diesel::result::Error> {
    use crate::db::schema::chat_message::dsl::{chat_message, subject};

    diesel::update(chat_message.find(id))
        .set(subject.eq(_subject))
        .returning(ChatMessage::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_creator_id_chat_message(
    conn: &mut PgConnection,
    id: i32,
    _creator_id: i32,
) -> Result<ChatMessage, diesel::result::Error> {
    use crate::db::schema::chat_message::dsl::{chat_message, creator_id};

    diesel::update(chat_message.find(id))
        .set(creator_id.eq(_creator_id))
        .returning(ChatMessage::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_message_body_chat_message(
    conn: &mut PgConnection,
    id: i32,
    _message_body: &'static str,
) -> Result<ChatMessage, diesel::result::Error> {
    use crate::db::schema::chat_message::dsl::{chat_message, message_body};

    diesel::update(chat_message.find(id))
        .set(message_body.eq(_message_body))
        .returning(ChatMessage::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_create_date_chat_message(
    conn: &mut PgConnection,
    id: i32,
    _create_date: chrono::NaiveDate,
) -> Result<ChatMessage, diesel::result::Error> {
    use crate::db::schema::chat_message::dsl::{chat_message, create_date};

    diesel::update(chat_message.find(id))
        .set(create_date.eq(_create_date))
        .returning(ChatMessage::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_chat_message_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::chat_message::dsl::{chat_message, id};

    diesel::delete(chat_message.filter(id.eq(_id))).execute(conn)
}
