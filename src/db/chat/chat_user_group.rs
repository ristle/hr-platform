#[cfg(feature = "ssr")]
use crate::msgs::chat::chat_user_group::{ChatUserGroup, NewChatUserGroup};
#[cfg(feature = "ssr")]
use chrono;
#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
pub fn _insert_chat_user_group(
    conn: &mut PgConnection,
    user_id: i32,
    group_id: i32,
    created_date: chrono::NaiveDate,
    is_active: bool,
) -> ChatUserGroup {
    use crate::db::schema::chat_user_group;

    let new_chat_user_group = NewChatUserGroup {
        user_id,
        group_id,
        created_date,
        is_active,
    };

    diesel::insert_into(chat_user_group::table)
        .values(&new_chat_user_group)
        .returning(ChatUserGroup::as_returning())
        .get_result(conn)
        .expect("Error saving new chat_user_group")
}
#[cfg(feature = "ssr")]
pub fn _find_chat_user_group(conn: &mut PgConnection, id: i32) -> Option<ChatUserGroup> {
    use crate::db::schema::chat_user_group::dsl::chat_user_group;

    let data = chat_user_group
        .find(id)
        .select(ChatUserGroup::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_user_id_chat_user_group(
    conn: &mut PgConnection,
    id: i32,
    _user_id: i32,
) -> Result<ChatUserGroup, diesel::result::Error> {
    use crate::db::schema::chat_user_group::dsl::{chat_user_group, user_id};

    diesel::update(chat_user_group.find(id))
        .set(user_id.eq(_user_id))
        .returning(ChatUserGroup::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_group_id_chat_user_group(
    conn: &mut PgConnection,
    id: i32,
    _group_id: i32,
) -> Result<ChatUserGroup, diesel::result::Error> {
    use crate::db::schema::chat_user_group::dsl::{chat_user_group, group_id};

    diesel::update(chat_user_group.find(id))
        .set(group_id.eq(_group_id))
        .returning(ChatUserGroup::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_created_date_chat_user_group(
    conn: &mut PgConnection,
    id: i32,
    _created_date: chrono::NaiveDate,
) -> Result<ChatUserGroup, diesel::result::Error> {
    use crate::db::schema::chat_user_group::dsl::{chat_user_group, created_date};

    diesel::update(chat_user_group.find(id))
        .set(created_date.eq(_created_date))
        .returning(ChatUserGroup::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_is_active_chat_user_group(
    conn: &mut PgConnection,
    id: i32,
    _is_active: bool,
) -> Result<ChatUserGroup, diesel::result::Error> {
    use crate::db::schema::chat_user_group::dsl::{chat_user_group, is_active};

    diesel::update(chat_user_group.find(id))
        .set(is_active.eq(_is_active))
        .returning(ChatUserGroup::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_chat_user_group_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::chat_user_group::dsl::{chat_user_group, id};

    diesel::delete(chat_user_group.filter(id.eq(_id))).execute(conn)
}
