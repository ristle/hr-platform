#[cfg(feature = "ssr")]
use crate::msgs::chat::chat_group::{ChatGroup, NewChatGroup};
#[cfg(feature = "ssr")]
use chrono;
#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
pub fn _insert_chat_group(
    conn: &mut PgConnection,
    group_name: &'static str,
    created_time: chrono::NaiveDate,
    is_active: bool,
) -> ChatGroup {
    use crate::db::schema::chat_group;

    let new_chat_group = NewChatGroup {
        group_name,
        created_time,
        is_active,
    };

    diesel::insert_into(chat_group::table)
        .values(&new_chat_group)
        .returning(ChatGroup::as_returning())
        .get_result(conn)
        .expect("Error saving new chat_group")
}

#[cfg(feature = "ssr")]
pub fn _find_chat_group(conn: &mut PgConnection, id: i32) -> Option<ChatGroup> {
    use crate::db::schema::chat_group::dsl::chat_group;

    let data = chat_group
        .find(id)
        .select(ChatGroup::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_group_name_chat_group(
    conn: &mut PgConnection,
    id: i32,
    _group_name: &'static str,
) -> Result<ChatGroup, diesel::result::Error> {
    use crate::db::schema::chat_group::dsl::{chat_group, group_name};

    diesel::update(chat_group.find(id))
        .set(group_name.eq(_group_name))
        .returning(ChatGroup::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_created_time_chat_group(
    conn: &mut PgConnection,
    id: i32,
    _created_time: chrono::NaiveDate,
) -> Result<ChatGroup, diesel::result::Error> {
    use crate::db::schema::chat_group::dsl::{chat_group, created_time};

    diesel::update(chat_group.find(id))
        .set(created_time.eq(_created_time))
        .returning(ChatGroup::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_is_active_chat_group(
    conn: &mut PgConnection,
    id: i32,
    _is_active: bool,
) -> Result<ChatGroup, diesel::result::Error> {
    use crate::db::schema::chat_group::dsl::{chat_group, is_active};

    diesel::update(chat_group.find(id))
        .set(is_active.eq(_is_active))
        .returning(ChatGroup::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_chat_group_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::chat_group::dsl::{chat_group, id};

    diesel::delete(chat_group.filter(id.eq(_id))).execute(conn)
}
