#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::resume::resume_skill::{NewResumeSkill, ResumeSkill};

#[cfg(feature = "ssr")]
pub fn _insert_resume_skill(
    conn: &mut PgConnection,
    resume_id: i32,
    skill_name: &'static str,
    experience_time: &'static str,
) -> ResumeSkill {
    use crate::db::schema::resume_skill;

    let new_resume_skill = NewResumeSkill {
        resume_id,
        skill_name,
        experience_time,
    };

    diesel::insert_into(resume_skill::table)
        .values(&new_resume_skill)
        .returning(ResumeSkill::as_returning())
        .get_result(conn)
        .expect("Error saving new resume_education")
}
#[cfg(feature = "ssr")]
pub fn _find_resume_skill(conn: &mut PgConnection, id: i32) -> Option<ResumeSkill> {
    use crate::db::schema::resume_skill::dsl::resume_skill;

    let data = resume_skill
        .find(id)
        .select(ResumeSkill::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_resume_id_resume_skill(
    conn: &mut PgConnection,
    id: i32,
    _resume_id: i32,
) -> Result<ResumeSkill, diesel::result::Error> {
    use crate::db::schema::resume_skill::dsl::{resume_id, resume_skill};

    diesel::update(resume_skill.find(id))
        .set(resume_id.eq(_resume_id))
        .returning(ResumeSkill::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_skill_name_resume_skill(
    conn: &mut PgConnection,
    id: i32,
    _skill_name: &'static str,
) -> Result<ResumeSkill, diesel::result::Error> {
    use crate::db::schema::resume_skill::dsl::{resume_skill, skill_name};

    diesel::update(resume_skill.find(id))
        .set(skill_name.eq(_skill_name))
        .returning(ResumeSkill::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_experience_time_resume_skill(
    conn: &mut PgConnection,
    id: i32,
    _experience_time: &'static str,
) -> Result<ResumeSkill, diesel::result::Error> {
    use crate::db::schema::resume_skill::dsl::{experience_time, resume_skill};

    diesel::update(resume_skill.find(id))
        .set(experience_time.eq(_experience_time))
        .returning(ResumeSkill::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_resume_skill_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::resume_skill::dsl::{id, resume_skill};

    diesel::delete(resume_skill.filter(id.eq(_id))).execute(conn)
}
