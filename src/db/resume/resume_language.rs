#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::resume::resume_language::{NewResumeLanguage, ResumeLanguage};

#[cfg(feature = "ssr")]
pub fn _insert_resume_language(
    conn: &mut PgConnection,
    resume_id: i32,
    language_name: &'static str,
    experience_time: &'static str,
) -> ResumeLanguage {
    use crate::db::schema::resume_language;

    let new_resume_language = NewResumeLanguage {
        resume_id,
        language_name,
        experience_time,
    };

    diesel::insert_into(resume_language::table)
        .values(&new_resume_language)
        .returning(ResumeLanguage::as_returning())
        .get_result(conn)
        .expect("Error saving new resume_education")
}
#[cfg(feature = "ssr")]
pub fn _find_resume_language(conn: &mut PgConnection, id: i32) -> Option<ResumeLanguage> {
    use crate::db::schema::resume_language::dsl::resume_language;

    let data = resume_language
        .find(id)
        .select(ResumeLanguage::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_resume_id_resume_language(
    conn: &mut PgConnection,
    id: i32,
    _resume_id: i32,
) -> Result<ResumeLanguage, diesel::result::Error> {
    use crate::db::schema::resume_language::dsl::{resume_id, resume_language};

    diesel::update(resume_language.find(id))
        .set(resume_id.eq(_resume_id))
        .returning(ResumeLanguage::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_language_name_resume_language(
    conn: &mut PgConnection,
    id: i32,
    _language_name: &'static str,
) -> Result<ResumeLanguage, diesel::result::Error> {
    use crate::db::schema::resume_language::dsl::{language_name, resume_language};

    diesel::update(resume_language.find(id))
        .set(language_name.eq(_language_name))
        .returning(ResumeLanguage::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_experience_time_resume_language(
    conn: &mut PgConnection,
    id: i32,
    _experience_time: &'static str,
) -> Result<ResumeLanguage, diesel::result::Error> {
    use crate::db::schema::resume_language::dsl::{experience_time, resume_language};

    diesel::update(resume_language.find(id))
        .set(experience_time.eq(_experience_time))
        .returning(ResumeLanguage::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_resume_language_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::resume_language::dsl::{id, resume_language};

    diesel::delete(resume_language.filter(id.eq(_id))).execute(conn)
}
