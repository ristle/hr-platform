#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::resume::resume_work::{NewResumeWork, ResumeWork};

#[cfg(feature = "ssr")]
pub fn _insert_resume_work(
    conn: &mut PgConnection,
    resume_id: i32,
    language_name: &'static str,
    details_info: Option<&'static str>,
    experience_time: &'static str,
) -> ResumeWork {
    use crate::db::schema::resume_work;

    let new_resume_work = NewResumeWork {
        resume_id,
        language_name,
        details_info,
        experience_time,
    };

    diesel::insert_into(resume_work::table)
        .values(&new_resume_work)
        .returning(ResumeWork::as_returning())
        .get_result(conn)
        .expect("Error saving new resume_work")
}
#[cfg(feature = "ssr")]
pub fn _find_resume_work(conn: &mut PgConnection, id: i32) -> Option<ResumeWork> {
    use crate::db::schema::resume_work::dsl::resume_work;

    let data = resume_work
        .find(id)
        .select(ResumeWork::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_resume_id_resume_work(
    conn: &mut PgConnection,
    id: i32,
    _resume_id: i32,
) -> Result<ResumeWork, diesel::result::Error> {
    use crate::db::schema::resume_work::dsl::{resume_id, resume_work};

    diesel::update(resume_work.find(id))
        .set(resume_id.eq(_resume_id))
        .returning(ResumeWork::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_language_name_resume_work(
    conn: &mut PgConnection,
    id: i32,
    _language_name: &'static str,
) -> Result<ResumeWork, diesel::result::Error> {
    use crate::db::schema::resume_work::dsl::{language_name, resume_work};

    diesel::update(resume_work.find(id))
        .set(language_name.eq(_language_name))
        .returning(ResumeWork::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_details_info_resume_work(
    conn: &mut PgConnection,
    id: i32,
    _details_info: Option<&'static str>,
) -> Result<ResumeWork, diesel::result::Error> {
    use crate::db::schema::resume_work::dsl::{details_info, resume_work};

    diesel::update(resume_work.find(id))
        .set(details_info.eq(_details_info))
        .returning(ResumeWork::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_experience_time_resume_work(
    conn: &mut PgConnection,
    id: i32,
    _experience_time: &'static str,
) -> Result<ResumeWork, diesel::result::Error> {
    use crate::db::schema::resume_work::dsl::{experience_time, resume_work};

    diesel::update(resume_work.find(id))
        .set(experience_time.eq(_experience_time))
        .returning(ResumeWork::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_resume_work_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::resume_work::dsl::{id, resume_work};

    diesel::delete(resume_work.filter(id.eq(_id))).execute(conn)
}
