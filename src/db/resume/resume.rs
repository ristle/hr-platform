#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::resume::resume::{NewResume, Resume};
#[cfg(feature = "ssr")]
pub fn _insert_resume(
    conn: &mut PgConnection,
    resume_name: &'static str,
    user_id: i32,
    photo_url: Option<&'static str>,
    job_name: &'static str,
    work_type: &'static str,
    work_experience: f64,
    salary_expectations: Option<i32>,
    location: Option<&'static str>,
    skill_id: i32,
    languages_id: i32,
    description: &'static str,
    work_id: i32,
    education_id: i32,
    lookup_vacancies: i32,
) -> Resume {
    use crate::db::schema::resume;

    let new_resume = NewResume {
        resume_name,
        user_id,
        photo_url,
        job_name,
        work_type,
        work_experience,
        salary_expectations,
        location,
        skill_id,
        languages_id,
        description,
        work_id,
        education_id,
        lookup_vacancies,
    };

    diesel::insert_into(resume::table)
        .values(&new_resume)
        .returning(Resume::as_returning())
        .get_result(conn)
        .expect("Error saving new post")
}
#[cfg(feature = "ssr")]
pub fn _find_resume(conn: &mut PgConnection, id: i32) -> Option<Resume> {
    use crate::db::schema::resume::dsl::resume;

    let data = resume
        .find(id)
        .select(Resume::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_resume_name_resume(
    conn: &mut PgConnection,
    id: i32,
    _resume_name: &'static str,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{resume, resume_name};

    diesel::update(resume.find(id))
        .set(resume_name.eq(_resume_name))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_user_id_resume(
    conn: &mut PgConnection,
    id: i32,
    _user_id: i32,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{resume, user_id};

    diesel::update(resume.find(id))
        .set(user_id.eq(_user_id))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_photo_url_resume(
    conn: &mut PgConnection,
    id: i32,
    _photo_url: Option<&'static str>,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{photo_url, resume};

    diesel::update(resume.find(id))
        .set(photo_url.eq(_photo_url))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_job_name_resume(
    conn: &mut PgConnection,
    id: i32,
    _job_name: &'static str,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{job_name, resume};

    diesel::update(resume.find(id))
        .set(job_name.eq(_job_name))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_work_type_resume(
    conn: &mut PgConnection,
    id: i32,
    _work_type: &'static str,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{resume, work_type};

    diesel::update(resume.find(id))
        .set(work_type.eq(_work_type))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_work_experience_resume(
    conn: &mut PgConnection,
    id: i32,
    _work_experience: f64,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{resume, work_experience};

    diesel::update(resume.find(id))
        .set(work_experience.eq(_work_experience))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_salary_expectations_resume(
    conn: &mut PgConnection,
    id: i32,
    _salary_expectations: Option<i32>,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{resume, salary_expectations};

    diesel::update(resume.find(id))
        .set(salary_expectations.eq(_salary_expectations))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_location_resume(
    conn: &mut PgConnection,
    id: i32,
    _location: Option<&'static str>,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{location, resume};

    diesel::update(resume.find(id))
        .set(location.eq(_location))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_skill_id_resume(
    conn: &mut PgConnection,
    id: i32,
    _skill_id: i32,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{resume, skill_id};

    diesel::update(resume.find(id))
        .set(skill_id.eq(_skill_id))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_languages_id_resume(
    conn: &mut PgConnection,
    id: i32,
    _languages_id: i32,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{languages_id, resume};

    diesel::update(resume.find(id))
        .set(languages_id.eq(_languages_id))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_description_resume(
    conn: &mut PgConnection,
    id: i32,
    _description: &'static str,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{description, resume};

    diesel::update(resume.find(id))
        .set(description.eq(_description))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_work_id_resume(
    conn: &mut PgConnection,
    id: i32,
    _work_id: i32,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{resume, work_id};

    diesel::update(resume.find(id))
        .set(work_id.eq(_work_id))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_education_id_resume(
    conn: &mut PgConnection,
    id: i32,
    _education_id: i32,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{education_id, resume};

    diesel::update(resume.find(id))
        .set(education_id.eq(_education_id))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_lookup_vacancies_resume(
    conn: &mut PgConnection,
    id: i32,
    _lookup_vacancies: i32,
) -> Result<Resume, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{lookup_vacancies, resume};

    diesel::update(resume.find(id))
        .set(lookup_vacancies.eq(_lookup_vacancies))
        .returning(Resume::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_resume_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::resume::dsl::{id, resume};

    diesel::delete(resume.filter(id.eq(_id))).execute(conn)
}
