#[cfg(feature = "ssr")]
use diesel::prelude::*;

#[cfg(feature = "ssr")]
use crate::msgs::resume::resume_education::{NewResumeEducation, ResumeEducation};

#[cfg(feature = "ssr")]
pub fn _insert_resume_education(
    conn: &mut PgConnection,
    resume_id: i32,
    education_name: &'static str,
    details_info: Option<&'static str>,
    experience_time: &'static str,
) -> ResumeEducation {
    use crate::db::schema::resume_education;

    let new_resume_education = NewResumeEducation {
        resume_id,
        education_name,
        details_info,
        experience_time,
    };

    diesel::insert_into(resume_education::table)
        .values(&new_resume_education)
        .returning(ResumeEducation::as_returning())
        .get_result(conn)
        .expect("Error saving new resume_education")
}
#[cfg(feature = "ssr")]
pub fn _find_resume_education(conn: &mut PgConnection, id: i32) -> Option<ResumeEducation> {
    use crate::db::schema::resume_education::dsl::resume_education;

    let data = resume_education
        .find(id)
        .select(ResumeEducation::as_select())
        .first(conn)
        .optional();

    match data {
        Ok(Some(data)) => Some(data),
        Ok(None) => None,
        Err(_) => None,
    }
}

#[cfg(feature = "ssr")]
pub fn _update_resume_id_resume_education(
    conn: &mut PgConnection,
    id: i32,
    _resume_id: i32,
) -> Result<ResumeEducation, diesel::result::Error> {
    use crate::db::schema::resume_education::dsl::{resume_education, resume_id};

    diesel::update(resume_education.find(id))
        .set(resume_id.eq(_resume_id))
        .returning(ResumeEducation::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_education_name_resume_education(
    conn: &mut PgConnection,
    id: i32,
    _education_name: &'static str,
) -> Result<ResumeEducation, diesel::result::Error> {
    use crate::db::schema::resume_education::dsl::{education_name, resume_education};

    diesel::update(resume_education.find(id))
        .set(education_name.eq(_education_name))
        .returning(ResumeEducation::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_details_info_resume_education(
    conn: &mut PgConnection,
    id: i32,
    _details_info: Option<&'static str>,
) -> Result<ResumeEducation, diesel::result::Error> {
    use crate::db::schema::resume_education::dsl::{details_info, resume_education};

    diesel::update(resume_education.find(id))
        .set(details_info.eq(_details_info))
        .returning(ResumeEducation::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _update_experience_time_resume_education(
    conn: &mut PgConnection,
    id: i32,
    _experience_time: &'static str,
) -> Result<ResumeEducation, diesel::result::Error> {
    use crate::db::schema::resume_education::dsl::{experience_time, resume_education};

    diesel::update(resume_education.find(id))
        .set(experience_time.eq(_experience_time))
        .returning(ResumeEducation::as_returning())
        .get_result(conn)
}

#[cfg(feature = "ssr")]
pub fn _delete_resume_education_by_id(
    conn: &mut PgConnection,
    _id: i32,
) -> Result<usize, diesel::result::Error> {
    use crate::db::schema::resume_education::dsl::{id, resume_education};

    diesel::delete(resume_education.filter(id.eq(_id))).execute(conn)
}
