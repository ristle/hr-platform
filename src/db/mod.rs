pub mod chat;
pub mod resume;
pub mod user;
pub mod vacancy;

#[cfg(feature = "ssr")]
pub mod schema;
