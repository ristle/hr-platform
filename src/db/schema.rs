// @generated automatically by Diesel CLI.

diesel::table! {
    chat_group (id) {
        id -> Int4,
        group_name -> Varchar,
        created_time -> Date,
        is_active -> Bool,
    }
}

diesel::table! {
    chat_message (id) {
        id -> Int4,
        subject -> Varchar,
        creator_id -> Int4,
        message_body -> Varchar,
        create_date -> Date,
    }
}

diesel::table! {
    chat_message_participant (id) {
        id -> Int4,
        recipient_id -> Int4,
        recipient_group_id -> Int4,
        message_id -> Int4,
        is_read -> Bool,
    }
}

diesel::table! {
    chat_user_group (id) {
        id -> Int4,
        user_id -> Int4,
        group_id -> Int4,
        created_date -> Date,
        is_active -> Bool,
    }
}

diesel::table! {
    platform_user (id) {
        id -> Int4,
        name -> Varchar,
        surname -> Varchar,
        email -> Varchar,
        password -> Varchar,
        role -> Varchar,
        photo_url -> Nullable<Varchar>,
        verified -> Bool,
        created_at -> Date,
        updated_at -> Date,
    }
}

diesel::table! {
    resume (id) {
        id -> Int4,
        resume_name -> Varchar,
        user_id -> Int4,
        photo_url -> Nullable<Varchar>,
        job_name -> Varchar,
        work_type -> Varchar,
        work_experience -> Float8,
        salary_expectations -> Nullable<Int4>,
        location -> Nullable<Varchar>,
        skill_id -> Int4,
        languages_id -> Int4,
        description -> Varchar,
        work_id -> Int4,
        education_id -> Int4,
        lookup_vacancies -> Int4,
    }
}

diesel::table! {
    resume_education (id) {
        id -> Int4,
        resume_id -> Int4,
        education_name -> Varchar,
        details_info -> Nullable<Varchar>,
        experience_time -> Varchar,
    }
}

diesel::table! {
    resume_language (id) {
        id -> Int4,
        resume_id -> Int4,
        language_name -> Varchar,
        experience_time -> Varchar,
    }
}

diesel::table! {
    resume_skill (id) {
        id -> Int4,
        resume_id -> Int4,
        skill_name -> Varchar,
        experience_time -> Varchar,
    }
}

diesel::table! {
    resume_work (id) {
        id -> Int4,
        resume_id -> Int4,
        language_name -> Varchar,
        details_info -> Nullable<Varchar>,
        experience_time -> Varchar,
    }
}

diesel::table! {
    vacancy (id) {
        id -> Int4,
        job_name -> Varchar,
        company_name -> Varchar,
        work_experience -> Float8,
        work_type -> Varchar,
        feature_id -> Int4,
        new_responses -> Int4,
        unread_responses -> Int4,
        skills -> Int4,
        details -> Varchar,
        vacancy_stages -> Int4,
        people_in_work_id -> Int4,
        related_people_id -> Int4,
    }
}

diesel::table! {
    vacancy_people_in_work (id) {
        id -> Int4,
        user_id -> Int4,
    }
}

diesel::table! {
    vacancy_people_related (id) {
        id -> Int4,
        user_id -> Int4,
    }
}

diesel::table! {
    vacancy_response (id) {
        id -> Int4,
        user_id -> Int4,
        vacancy_id -> Int4,
        status -> Varchar,
        vacancy_stage_id -> Int4,
        hr_user_id -> Int4,
        is_read -> Bool,
    }
}

diesel::table! {
    vacancy_skill (id) {
        id -> Int4,
        user_id -> Int4,
        name -> Varchar,
        experience_time -> Float8,
    }
}

diesel::table! {
    vacancy_stage (id) {
        id -> Int4,
        vacancy_stage_id -> Int4,
        name -> Varchar,
        is_active -> Bool,
    }
}

diesel::table! {
    vacancy_stages (id) {
        id -> Int4,
        name -> Varchar,
    }
}

diesel::allow_tables_to_appear_in_same_query!(
    chat_group,
    chat_message,
    chat_message_participant,
    chat_user_group,
    platform_user,
    resume,
    resume_education,
    resume_language,
    resume_skill,
    resume_work,
    vacancy,
    vacancy_people_in_work,
    vacancy_people_related,
    vacancy_response,
    vacancy_skill,
    vacancy_stage,
    vacancy_stages,
);
