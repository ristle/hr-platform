pub mod home;
pub mod login;
pub mod logo;
pub mod signup;

pub mod start_page;
pub mod central_panel;
pub mod profile_info;