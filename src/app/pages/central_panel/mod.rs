use leptos::*;

mod side_bar;
use side_bar::SideBar;

use crate::app::pages::profile_info::ProfileInfo;
use crate::app::user_tabs::user_trait::UserTrait;

#[component]
pub fn CentralPanel() -> impl IntoView {
    let callback = move |_: web_sys::MouseEvent| {
        web_sys::window()
            .unwrap()
            .document()
            .unwrap()
            .get_element_by_id("sidebar")
            .unwrap()
            .class_list()
            .toggle("toggled")
            .unwrap();
    };

    let candidate = crate::app::user_tabs::candidate::Candidate::new();
    view! {
      <div class="layout has-sidebar fixed-sidebar fixed-header" >
            <SideBar candidate=candidate.clone()/>
            <div class="overlay" id="overlay" on:click=callback> </div>
        <div class="profile-context">
            <ProfileInfo />
             {candidate.get_current_panel()}
        </div>
    </div>
    }
}
