use crate::app::user_tabs::user_trait::UserTrait;
use leptos::*;
use web_sys::MouseEvent;

fn side_bar_collapse_callback() {
    let side_bar = web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .get_element_by_id("sidebar")
        .unwrap();
    side_bar.clone().class_list().toggle("collapsed").unwrap();

    web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .get_element_by_id("sidebar-layout")
        .unwrap()
        .class_list()
        .toggle("collapsed")
        .unwrap();
    web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .get_element_by_id("sidebar-layout")
        .unwrap()
        .remove_attribute("min-width")
        .unwrap();

    if side_bar.clone().class_list().contains("collapsed") {
        let some_query = web_sys::window()
            .unwrap()
            .document()
            .unwrap()
            .query_selector_all(".menu > ul > .menu-item.sub-menu > a")
            .unwrap();
        for s in 0..some_query.length() {
            let some_iter = some_query.get(s).unwrap();
            web_sys::window()
                .unwrap()
                .document()
                .unwrap()
                .get_element_by_id(some_iter.node_name().as_str())
                .unwrap()
                .parent_element()
                .unwrap()
                .class_list()
                .remove_1("open")
                .unwrap();
        }

        for s in 0..some_query.length() {
            let some_iter = some_query.get(s).unwrap();
            web_sys::window()
                .unwrap()
                .document()
                .unwrap()
                .get_element_by_id(some_iter.node_name().as_str())
                .unwrap()
                .first_element_child()
                .unwrap()
                .class_list()
                .remove_1("open")
                .unwrap();
        }
    }
}

#[component]
pub fn SideBar<UserData: UserTrait + Clone>(candidate: UserData) -> impl IntoView {
    let sidebar_show = move |_: MouseEvent| side_bar_collapse_callback();
    let candidate = candidate.clone();

    view! {
      <div id="sidebar" class="sidebar break-point-sm has-bg-image">
          <a id="btn-collapse" class="sidebar-collapser" on:click=sidebar_show>
              <i class="ri-arrow-left-s-line">
                  <leptos_icons::Icon icon=leptos_icons::Icon::from(leptos_icons::ChIcon::ChArrowLeft)/>
              </i>
          </a>
          <div id="sidebar-layout" class="sidebar-layout">
              <div class="sidebar-header">
                  <div class="pro-sidebar-logo">
                      <leptos_icons::Icon icon=leptos_icons::Icon::from(leptos_icons::CgIcon::CgArrowsExchange) height="36px" width="36px"/>
                      <h5>{"In Reverse"}</h5>
                  </div>
              </div>
              <div class="sidebar-content">
                  <nav  class="side-bar-navbar menu open-current-submenu" >
                    <ul>
                        {candidate.get_side_buttons()}
                    </ul>
                </nav>
              </div>

          </div>
      </div>
    }
}
