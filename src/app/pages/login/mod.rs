use gloo_storage::{LocalStorage, Storage};
use leptos::*;
use leptos_router::*;

use crate::app::pages::start_page::StartPage;
use crate::app::utils::auth::*;
use crate::app::utils::notification::structs::{
    Notification, NotificationMessage, NotificationType, Notifications,
};


#[cfg(feature = "ssr")]
use crate::msgs::ServerData;

#[server(CheckLoginData, "/api", "Url", "login")]
pub async fn check_login_data(username: String, password: String) -> Result<(), ServerFnError> {
    use crate::db::user::find_new_user_by_email;
    use actix_web::{cookie::{time::Duration as ActixWebDuration, Cookie},
                    http::header, http::header::HeaderValue};
    use leptos_actix::ResponseOptions;

    let login_data = Credentials { username, password };

    logging::log!("Login User");
    dbg!(login_data.clone());

    leptos_actix::extract(
        move |data: actix_web::web::Data<tokio::sync::Mutex<ServerData>>| async move {
            let mut data = data.lock().await;

            logging::log!("Logging Log");

            let user = find_new_user_by_email(&mut data.connection, login_data.username);


            // pull ResponseOptions from context
            let response = expect_context::<ResponseOptions>();

            match user {
                Some(user) => {
                    let user_data = serde_json::to_string(&user).unwrap();

                    dbg!(user_data.clone());

                    data.user_data.insert("user", user_data);
                    logging::log!("Logging user... Redirecting to /");

                    // JWT
                    use crate::server_functions::auth::TokenClaims;
                    use chrono::{prelude::*, Duration};
                    use jsonwebtoken::{encode, EncodingKey, Header};

                    let now = Utc::now();
                    let iat = now.timestamp() as usize;
                    let exp = (now + Duration::minutes(60)).timestamp() as usize;
                    let claims: TokenClaims = TokenClaims {
                        sub: user.id.to_string(),
                        exp,
                        iat,
                    };

                    let token = encode(
                        &Header::default(),
                        &claims,
                        &EncodingKey::from_secret(data.env.jwt_secret.as_ref()),
                    )
                    .unwrap();

                    let cookie = Cookie::build("token", token.to_owned())
                        .path("/")
                        .max_age(ActixWebDuration::new(60 * 60, 0))
                        .http_only(true)
                        .finish();

                    if let Ok(cookie) =  HeaderValue::from_str(&cookie.to_string()){
                        response.insert_header(header::SET_COOKIE, cookie);
                    }
                    // End of JWT Part

                    Ok(())
                }
                None => {
                    logging::log!("Failed log user... Redirecting to /");

                    // set the HTTP status code
                    response.set_status(http::StatusCode::IM_A_TEAPOT);
                    Err(ServerFnError::ServerError("Users not found".to_string()))
                }
            }
        },
    )
    .await?
}

#[component]
fn LoginForm(notification_write_signal: WriteSignal<Notifications>) -> impl IntoView {
    let login = create_server_multi_action::<CheckLoginData>();
    let _submissions = login.submissions();

    // list of todos is loaded from the server in reaction to changes
    let users = create_resource(move || (login.version().get()), move |_| get_user());
    let nav = leptos_router::use_navigate();

    view! {
        <MultiActionForm class="auth-form active " action=login on:submit=move |ev| {
                let _data = CheckLoginData::from_event(&ev).expect("to parse form data");
                }>

                <div class="flex flex-col justify-center gap-8-px mb-4">

                    <div class="auth-form__header mb-[10px]">
                        <h1 class="text-2xl font-bold">"Вход в аккаунт"</h1>
                        <p class=" text-sm not-italic font-normal leading-[150%]; gap-1">"Ещё не с нами?" <button id="sign-up-btn" type="button" on:click=move |_|  nav("/signup", leptos_router::NavigateOptions::default())><u>{"Зарегистрироваться"}</u></button></p>

                    </div>

                    <label class="form-group">
                        <label for="username">{"Email address"}</label>
                        <input type="email" id="username" name="username" class="rounded-full bg-[#EDF1FC] max-w-lg h-10 max-h-14 shrink-0" placeholder="subscribe@ristle.ru" aria-label="Email address" required=true />

                        <label for="password">{"Password"}</label>
                        <input type="password" required=true id="password" name="password" class="rounded-full bg-[#EDF1FC] max-w-lg h-10 max-h-14 shrink-0" minlength="8" aria-label="Password"/>
                    </label>

                    <input type="submit" value="Войти" class=" w-[170px] h-[45px] rounded-full font-['Roboto']  not-italic font-semibold \
                            text-[16px] leading-[23px] flex items-center text-center justify-center \
                            text-[#FFFFFF] bg-[#4977EA] border-[none] text-b-white"/>
                </div>
                <Transition fallback=move || view! {<p>"Loading..."</p> }>
                       {
                          move || {
                               let _ = if let Some(data) = users.get() {
                                    match data {
                                        Ok(auth_user) => {
                                            let nav = leptos_router::use_navigate();

                                            LocalStorage::set("auth_user", auth_user).unwrap();
                                            nav("/profile", leptos_router::NavigateOptions::default());
                                       },
                                        Err(error_message) => {
                                            dbg!("Error message update");
                                            notification_write_signal.update(|n: &mut Notifications| {
                                                let mut old_n = (*n).notifications.clone();
                                                old_n.push(Notification::new(
                                                    NotificationMessage {
                                                        type_: NotificationType::Error,
                                                        body: error_message.to_string()
                                                    })
                                                );

                                                *n = Notifications::new(old_n);
                                            });
                                        }
                                    }
                               };
                                view!{}
                           }
                       }
                </Transition>
        </MultiActionForm>
    }
}

#[component]
pub fn Login(notification_write_signal: WriteSignal<Notifications>) -> impl IntoView {
    leptos_meta::provide_meta_context();

    view! {
        <StartPage input_form={view! {
            <LoginForm notification_write_signal=notification_write_signal/>
        }
        }/>
    }
}
