use crate::app::pages::logo::Logo;
use leptos::*;

#[component]
pub fn StartPage<T: IntoView>(input_form: T) -> impl IntoView {
    leptos_meta::provide_meta_context();

    view! {
        <div class="page-container">
            <Logo />
            {input_form}
            <img src={"/background.svg"} class="flex flex-row  m-[5%] rounded-[20px] \
                                                [@media(width<930px)]:hidden justify-center" />
        </div>
    }
}
