use gloo_storage::{LocalStorage, Storage};
use leptos::*;
use leptos_router::*;

use crate::app::pages::start_page::StartPage;
use crate::app::utils::auth::*;
use crate::app::utils::notification::structs::{
    Notification, NotificationMessage, NotificationType, Notifications,
};
use leptos_use::{
    use_web_notification_with_options, NotificationDirection, ShowOptions,
    UseWebNotificationOptions, UseWebNotificationReturn,
};

#[server(SignUpNewUser, "/api", "Url", "signup")]
async fn signup_new_user(
    name: String,
    surname: String,
    email: String,
    role: String,
    password: String,
    password_confirm: String,
) -> Result<(), ServerFnError> {
    use crate::db::user::insert_new_user;

    let new_platform_user_json = crate::msgs::user::NewPlatformUserJson {
        name: name.clone(),
        surname: surname.clone(),
        email: email.clone(),
        role: role.clone(),
        password: password.clone(),
    };

    logging::log!("Singup User");
    dbg!(new_platform_user_json);

    leptos_actix::extract(
        move |data: actix_web::web::Data<tokio::sync::Mutex<crate::msgs::ServerData>>| async move {
            let mut data = data.lock().await;

            logging::log!("Logging Log");

            if password != password_confirm {
                return Err(ServerFnError::Registration(
                    "Password didn't match".to_string(),
                ));
            }

            let user = insert_new_user(
                &mut data.connection,
                name,
                surname,
                email,
                password,
                role,
                "".to_string(),
            );

            let user_data = serde_json::to_string(&user).unwrap();

            dbg!(user_data.clone());

            data.user_data.insert("user", user_data);

            Ok(())
        },
    )
    .await?
}
#[component]
fn Demo() -> impl IntoView {
    let UseWebNotificationReturn {
        is_supported, show, ..
    } = use_web_notification_with_options(
        UseWebNotificationOptions::default()
            .title("Test Notification")
            .direction(NotificationDirection::Auto)
            .language("ru")
            // .renotify(true)
            .tag("In Reverse"),
    );

    let show = move || {
        show(ShowOptions::default());
    };

    view! {
        <Show
            when=is_supported
            fallback=|| {
                view! { <div>The Notification Web API is not supported in your browser.</div> }
            }
        >

            <button on:click={
                let show = show.clone();
                move |_| show()
            }>Show Notification</button>
        </Show>
    }
}

#[component]
fn SignUpForm(notification_write_signal: WriteSignal<Notifications>) -> impl IntoView {
    let signup = create_server_multi_action::<SignUpNewUser>();
    let _submissions = signup.submissions();

    // list of todos is loaded from the server in reaction to changes
    let users = create_resource(move || (signup.version().get()), move |_| get_user());

    let nav = leptos_router::use_navigate();

    leptos_meta::provide_meta_context();

    view! {
         <MultiActionForm class="auth-form active " action=signup on:submit=move |ev| {
                let _data = SignUpNewUser::from_event(&ev).expect("to parse form data");
                }>

                <div class="flex flex-col justify-center gap-8-px mb-4">

                    <div class="auth-form__header mb-[10px]">
                        <h1 class="text-2xl font-bold">"Регистрация"</h1>
                        <p class=" text-sm not-italic font-normal leading-[150%];">"Уже есть аккаут? " <button id="sign-up-btn" type="button" on:click=move |_|  nav("/login", leptos_router::NavigateOptions::default())><u>{"Войти"}</u></button></p>

                    </div>

                    <label class="form-group justify-items-stretch">
                        <div class="flex flex-row justify-items-stretch  space-x-2">
                            <div class="flex flex-col gap-1">
                                <label for="name">{"Имя"}</label>
                                <input type="text" id="username" name="name" class="rounded-full bg-[#EDF1FC] max-w-lg h-10 max-h-14 shrink-0" placeholder="Станислав" aria-label="Email address" required=true />
                            </div>

                            <div class="flex flex-col gap-1">
                                <label for="surname">{"Фамилия"}</label>
                                <input type="text" required=true id="surname" name="surname" class="rounded-full bg-[#EDF1FC] max-w-lg h-10 max-h-14 shrink-0"  placeholder="Макаров"  required=true />
                            </div>
                        </div>

                        <div class="flex flex-col justify-items-stretch gap-1">
                            <label for="email">{"Email"}</label>
                            <input type="email" required=true id="email" name="email" class="rounded-full bg-[#EDF1FC] max-w-lg h-10 max-h-14 shrink-0" required=true />
                        </div>

                        <div class="flex flex-col gap-1">
                            <label for="role">{"Роль"}</label>
                            <input type="text" name="role" id="role" list="role_name" reqired=true
                                class="rounded-full bg-[#EDF1FC] max-w-lg h-10 max-h-14 shrink-0"/>
                            <datalist id="role_name">
                              <option value="HR"/>
                              <option value="HRBP"/>
                              <option value="Candidate"/>
                              <option value="Company"/>
                            </datalist>
                        </div>

                        <div class="flex flex-row justify-items-stretch space-x-2 ">
                            <div class="flex flex-col gap-1">
                                <label for="password">{"Пароль"}</label>
                                <input type="password" required=true id="password" name="password" class="rounded-full bg-[#EDF1FC] max-w-lg h-10 max-h-14 shrink-0" minlength="8" aria-label="Password" required=true />
                            </div>
                            <div class="flex flex-col gap-1">
                                <label for="password_confirm">{"Подтверждение пароля"}</label>
                                <input type="password" required=true id="password_confirm" name="password_confirm" class="rounded-full bg-[#EDF1FC] max-w-lg h-10 max-h-14 shrink-0" minlength="8" aria-label="Password" required=true />
                            </div>
                        </div>

                    </label>

                    <input type="submit" value="Зарегистрироваться" class=" w-[170px] h-[45px] rounded-full font-['Roboto']  not-italic font-semibold \
                            text-[16px] leading-[23px] flex items-center text-center justify-center \
                            text-[#FFFFFF] bg-[#4977EA] border-[none] text-b-white"/>

                </div>
                <Suspense fallback=move || view! {<p>"Loading..."</p> }>
                       {
                          move || {
                               let _ = if let Some(data) = users.get() {
                                    match data {
                                        Ok(auth_user) => {
                                            LocalStorage::set("auth_user", Some(auth_user)).unwrap();
                                            let nav = leptos_router::use_navigate();
                                            nav("/profile", leptos_router::NavigateOptions::default());
                                       },
                                        Err(error_message) => {
                                            notification_write_signal.update(|n: &mut Notifications| {
                                                let mut old_n = (*n).notifications.clone();
                                                old_n.push(Notification::new(
                                                        NotificationMessage {
                                                            type_: NotificationType::Error,
                                                            body: error_message.to_string()
                                                        }
                                                    )
                                                );

                                                *n = Notifications::new(old_n);
                                            });
                                        }
                                    }
                               };
                               view!{
                               }
                           }
                       }
                </Suspense>
        </MultiActionForm>
    }
}

#[component]
pub fn SignUp(notification_write_signal: WriteSignal<Notifications>) -> impl IntoView {
    leptos_meta::provide_meta_context();

    view! {
        <StartPage input_form={view! {
                    <SignUpForm notification_write_signal=notification_write_signal/>
            }
        }/>
    }
}
