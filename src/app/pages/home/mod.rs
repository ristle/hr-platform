use leptos::*;

#[component]
fn TextPart() -> impl IntoView {
    view! {
        <div class=" font-sans  text-base not-italic font-normal not-italic font-medium text-base text-black">
            <h2 class="text-2xl font-sans not-italic font-semibold">
                {"Мы стремимся быть лучшими везде и во всем, приглашаем и тебя!"}
            </h2>

            <div class="mt-6 space-y-2 font-sans  text-base not-italic font-normal not-italic \
                        font-medium text-base text-black">
                <p class="font-sans md:font-serif"> <strong>{"Для Соискателей"}</strong></p>
                <p class="font-sans indent-0">
                    {"После регистрации — получи доступ к секретным и крутым вакансиям от нашей компании"}
                </p>

                <ul class="list-disc list-inside font-sans md:font-serif">
                    <li class="font-sans md:font-serif">{"Откликайся на вакансии"}</li>
                    <li class="font-sans md:font-serif">{"Следи за этапами рассмотрения твоего резюме"}</li>
                    <li class="font-sans md:font-serif">{"Прямой чат с HR"}</li>
                </ul>
            </div>

            <div class="mt-6 space-y-2 font-sans  text-base not-italic font-normal \
                        not-italic font-medium text-base text-black">
                <p class="font-sans md:font-serif"><strong>{"Для Сотрудников"}</strong></p>
                <p class="indent-0 space-y-8 font-sans md:font-serif">
                    {"Автоматизированная система создания заявок, описания вакансий"}
                </p>

                <ul class="list-disc list-inside  font-sans md:font-serif">
                    <li>{"Единая база и резерв резюме"}</li>
                    <li>{"
                        Связывайтесь с соискателями не переходя на другие сервисы"}</li>
                    <li>{"
                        Отслеживайте все этапы работы над вакансией"}</li>
                    <li>{"Коммуникация с командой, формы утверждения кандидата, просмотр статистики и многое другое!"}</li>
                </ul>
            </div>
        </div>

    }
}

#[component]
fn FormFirstLoginPart() -> impl IntoView {
    let nav = leptos_router::use_navigate();

    view! {
        <form  class="mt-6"  action="" id="checkbox" /* onclick={login.clone()}*/ >
            <div class="flex flex-col gap-8-px mb-4  self-end">
                <div class="flex mt-20-px thead mb-15-px items-center justify-start gap-3 rounded-full">
                    <input id="link-checkbox" type="checkbox" class="w-6 h-6 rounded-full border-[#4977EA]"/>
                    <label  for="link-checkbox"  class="text-[#4977EA] text-[16px] rounded-full">
                        {"Я согласен на "}
                        <a href="https://roskazna.gov.ru/anticorruption/doc/soglasie.doc">
                            {"обработку персональных данных"}
                        </a>
                    </label>

                </div>
                <button class=" h-[60px] mt-[10px] rounded-full font-['Roboto'] not-italic \
                                font-semibold text-[16px] leading-[23px] flex items-center \
                                text-center justify-center text-[#FFFFFF] bg-[#4977EA] border-[none]"
                        on:click=move |_|  nav("/lo\
                        gin", leptos_router::NavigateOptions::default())>
                    {"Войти"}
                </button>
            </div>
        </form>
    }
}

#[component]
fn FormForRegistration() -> impl IntoView {
    let nav = leptos_router::use_navigate();
    let nav_clone = leptos_router::use_navigate();
    view! {
        <div class="flex flex-col gap-8-px mb-4 ">
            <button class=" h-[60px] mt-[10px] rounded-full font-['Roboto'] not-italic \
                            font-semibold text-[16px] leading-[23px] flex items-center \
                            text-center justify-center text-[#FFFFFF] bg-[#4977EA] border-[none]"
                    on:click=move |_|  nav("/login", leptos_router::NavigateOptions::default())>
                        {"Войти"}
            </button>
            <button class=" h-[60px] mt-[10px] rounded-full font-['Roboto'] not-italic font-semibold \
                            text-[16px] leading-[23px] flex items-center text-center justify-center \
                            text-[#FFFFFF] bg-[#EDF1FC] border-[none] text-b-blue"
                    on:click=move |_|  nav_clone("/signup", leptos_router::NavigateOptions::default())>
                    {"Еще не с нами? "} <u class="m-1">{"Регистрация"}</u>
            </button>
        </div>
    }
}

#[component]
fn BodyPart(by_link: bool) -> impl IntoView {
    view! {
        <div class="flex flex-row m-[2%] shadow-[4px_4px_50px_rgba(112,116,126,0.15)] pt-0.5 \
                    content-center  rounded-[20px] \
                    [@media(width < 576px)]:pl-20 [@media(width < 576px)]:pr-10">
            <div class="flex flex-col m-[2%] font-sans md:font-serif place-content-between">
                <TextPart />
                {move || if by_link {
                        view! { <FormFirstLoginPart/> }
                    } else {
                        view! { <FormForRegistration/> }
                    }
                }
            </div>
            <img src={"/background.svg"} class="relative w-[550px] h-[691px] m-[30px] \
                                                rounded-[20px] [@media(width<930px)]:hidden" />
        </div>
    }
}

#[component]
pub fn HomePage(by_link: bool) -> impl IntoView {
    view! {
        <div class="relative table flex-nowrap content-center justify-around items-center mt-[2%] mx-auto">
            <div class=" flex flex-col flex-nowrap content-center justify-around items-center">

                <p class="font-sans text-[xxx-large] text-center font-semibold leading-[47px] flex items-center border-[none]">{"Добро пожаловать в InReverse"}</p>

                <BodyPart by_link=by_link/>
            </div>
        </div>

    }
}
