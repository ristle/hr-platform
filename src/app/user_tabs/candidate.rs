use crate::app::user_tabs::user_trait::*;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use leptos::html::Div;
use leptos::*;

use crate::app::utils::side_button::{SideButton, StartUpButtonStatus};

#[derive(Clone)]
pub struct Candidate {
    pub panel_data: CentralPanelData,
}

lazy_static::lazy_static!(
    pub static ref CURRENT_ACTIVE_BUTTON: Arc::<Mutex::<WriteSignal<String>>> = {
        let (_, write_signal) = leptos::create_signal("".to_string());

        Arc::new(Mutex::new(write_signal))
    };
);

impl Candidate {
    fn get_related_vacancy_panel() -> HtmlElement<Div> {
        view! {
          <div> <p>"Подходящие вакансии" </p> </div>
        }
    }

    fn get_related_response_panel() -> HtmlElement<Div> {
        view! {
          <div> <p>"Отклики и приглашения" </p> </div>
        }
    }

    fn get_related_favorites_panel() -> HtmlElement<Div> {
        view! {
          <div> <p>"Избранное" </p> </div>
        }
    }

    fn update_active_button_status(value: &mut String) {
        if (*value).contains("active") {
            let mut v: Vec<char> = (*value).chars().collect::<Vec<char>>();

            v.splice(24..30, vec![]);

            *value = v.iter().collect::<String>();
        } else {
            *value += " active";
        }
    }

    fn create_button(&self, button_data: SideButton) -> impl IntoView {
        let set_active_tab = self.panel_data.set_active_tab_write_signal.clone();
        let name = button_data.name;

        let tab_to_set = button_data.tab_to_set.clone();
        let write_button_active = button_data.write_signal.clone();
        let read_button_signal = button_data.read_signal.clone();

        let update_active_tabs = move || {
            let mut current_button = CURRENT_ACTIVE_BUTTON.lock().unwrap();

            write_button_active.update(|value| Candidate::update_active_button_status(value));

            (*current_button).update(|value| Candidate::update_active_button_status(value));
            *current_button = write_button_active.clone();
            set_active_tab.set(tab_to_set());
        };

        if let StartUpButtonStatus::Active = button_data.startup_status {
            update_active_tabs();
        }

        view! {
            <div class={"super-tab-button-section"}>
                <button id=name
                    on:click=move |_| update_active_tabs()
                    class=read_button_signal>
                    {name}
                </button>
            </div>
        }
    }
}

impl UserTrait for Candidate {
    fn new() -> Candidate {
        let (get_active_tab_read_signal, set_active_tab_write_signal) =
            create_signal(view! {<div> </div>});
        let mut all_buttons = HashMap::new();

        all_buttons.insert(
            "Подходящие вакансии".to_string(),
            SideButton::new("Подходящие вакансии", Candidate::get_related_vacancy_panel, StartUpButtonStatus::None),
        );
        all_buttons.insert(
            "Отклики и приглашения".to_string(),
            SideButton::new(
                "Отклики и приглашения",
                Candidate::get_related_response_panel,
                StartUpButtonStatus::Active
            ),
        );
        all_buttons.insert(
            "Избранное".to_string(),
            SideButton::new("Избранное", Candidate::get_related_favorites_panel, StartUpButtonStatus::None),
        );

        let panel_data = CentralPanelData {
            get_active_tab_read_signal,
            set_active_tab_write_signal,
            all_buttons,
        };

        Candidate { panel_data }
    }

    fn get_side_buttons(&self) -> impl IntoView {
        view! {
            {
                self.panel_data.all_buttons
                    .clone()
                    .into_iter()
                    .map(|(_, side_button)| {
                        self.create_button(side_button)
                    })
                    .collect::<Vec<_>>()
            }
        }
    }

    fn get_current_panel(&self) -> impl IntoView {
        let signal = self.panel_data.get_active_tab_read_signal.clone();
        view! {
            <div>{signal}</div>
        }
    }
}
