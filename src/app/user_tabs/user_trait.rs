use crate::app::utils::side_button::SideButton;

use leptos::html::Div;
use leptos::*;

#[derive(Clone)]
pub struct CentralPanelData {
    pub get_active_tab_read_signal: ReadSignal<HtmlElement<Div>>,
    pub set_active_tab_write_signal: WriteSignal<HtmlElement<Div>>,
    pub all_buttons: std::collections::HashMap<String, SideButton>,
}

pub trait UserTrait {
    fn new() -> Self;

    fn get_side_buttons(&self) -> impl IntoView;
    fn get_current_panel(&self) -> impl IntoView;
}
