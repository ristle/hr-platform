pub mod pages;
pub mod user_tabs;
pub mod utils;

use leptos::*;
use leptos_meta::*;
use leptos_router::*;

use crate::app::utils::notification::{structs::Notifications, NotificationsPage};
use pages::central_panel::CentralPanel;
use pages::home::HomePage;
use pages::login::Login;
use pages::signup::SignUp;

#[component]
pub fn App() -> impl IntoView {
    provide_meta_context();
    let (notification_read_signal, notification_write_signal) =
        create_signal(Notifications::new(vec![]));

    view! {
      <Stylesheet id="leptos" href="/pkg/hr-platform.css"/>
      // sets the document title
      <Title text="HR Platform"/>

      <NotificationsPage
            notification_read_signal=notification_read_signal
            notification_write_signal=notification_write_signal/>

      <Router>
        <Routes>
          <Route path="/" view=move || {view! { <HomePage by_link=false/> }}/>
          <Route path="/link_example" view=move || {view! {<HomePage by_link=true/>}}/>
          <Route path="/profile" view=CentralPanel/>
          <Route path="/login" view=move || {
        view! {
          <Login notification_write_signal=notification_write_signal.clone()/>
        }
      }/>/>
          <Route path="/signup" view=move || {
            view! {
              <SignUp notification_write_signal=notification_write_signal.clone()/>
            }
          }/>
        </Routes>
      </Router>
    }
}
