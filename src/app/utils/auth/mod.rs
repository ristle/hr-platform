use leptos::*;
use serde::{Deserialize, Serialize};

use crate::msgs::user::PlatformUser;

#[cfg(feature = "ssr")]
use crate::msgs::ServerData;

#[derive(Deserialize, Debug, Clone)]
pub struct Credentials {
    pub username: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct User {
    pub id: i64,
    pub username: String,
    pub password: String,
}

#[server(GetUser, "/api", "Url", "get")]
pub async fn get_user() -> Result<Option<PlatformUser>, ServerFnError> {
    logging::log!("Getting User...");

    leptos_actix::extract(
        move |data: actix_web::web::Data<tokio::sync::Mutex<ServerData>>| async move {
            let cookie = &data.lock().await.user_data;

            match cookie.get("user") {
                Some(data) => match serde_json::from_str::<PlatformUser>(data.as_str()) {
                    Ok(user) => {
                        dbg!(user.clone());
                        Ok(Some(user))
                    }
                    Err(_error) => Ok(None),
                },
                None => Err(ServerFnError::ServerError(
                    "Error in parsing user".to_string(),
                )),
            }
        },
    )
    .await?
}
