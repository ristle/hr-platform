use leptos::*;

use leptos_icons::AiIcon::*;
use leptos_icons::*;
use serde::{Deserialize, Serialize};

use std::time::Duration;

#[derive(Clone, Debug, Serialize, Deserialize, Hash, PartialEq, Eq, Default)]
pub enum NotificationType {
    Success,
    #[default]
    Notice,
    Warning,
    Error,
}

#[derive(Clone, Debug, Serialize, Deserialize, Hash, PartialEq, Eq, Default)]
pub struct NotificationMessage {
    pub type_: NotificationType,
    pub body: String,
}

impl NotificationMessage {
    fn type_to_string(&self) -> String {
        match self.type_ {
            NotificationType::Success => "success",
            NotificationType::Notice => "notice",
            NotificationType::Warning => "warning",
            NotificationType::Error => "error",
        }
        .to_string()
    }

    fn get_class_attribute(&self) -> String {
        match self.type_ {
            NotificationType::Success => "shadow-[#33653538]",
            NotificationType::Notice => "shadow-[#145b7b73]",
            NotificationType::Warning => "shadow-[#876b1573]",
            NotificationType::Error => "shadow-[#8f312f70]",
        }
        .to_string()
    }
    pub fn to_html(&self) -> impl IntoView {
        let icon = match self.type_ {
            NotificationType::Success => {
                view! {<Icon icon=Icon::from(AiCheckCircleOutlined) width="2em" height="2em"/>}
            }
            NotificationType::Notice => {
                view! {<Icon icon=Icon::from(AiCheckCircleOutlined) width="2em" height="2em"/>}
            }
            NotificationType::Warning => {
                view! {<Icon icon=Icon::from(AiCheckCircleOutlined) width="2em" height="2em"/>}
            }
            NotificationType::Error => {
                view! {<Icon icon=Icon::from(AiCheckCircleOutlined) width="2em" height="2em"/>}
            }
        };

        view! {
          <div class=self.get_class_attribute() + "notify top-left notify do-show gap-1 shadow-2xl z-100"
                 data-notification-status={self.type_to_string()}>
              {icon}
              <span>{self.body.clone()}</span>
            </div>
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Notification {
    pub message: NotificationMessage,
    pub show: ReadSignal<bool>,
}

impl Notification {
    pub fn new(message: NotificationMessage) -> Notification {
        let (read_signal, write_signal) = create_signal(true);

        create_effect(move |_| {
            set_timeout(
                move || {
                    logging::log!("Setting defaul to notification");
                    write_signal(false);
                },
                Duration::from_secs(5),
            );
        });

        Notification {
            message,
            show: read_signal,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Notifications {
    pub notifications: Vec<Notification>,
}

impl Notifications {
    pub fn new(notifications: Vec<Notification>) -> Notifications {
        Notifications { notifications }
    }
}
