use leptos::*;

use std::time::Duration;

pub mod structs;
use structs::*;

async fn filter_notifications(inp: Notifications) -> Vec<Notification> {
    inp.notifications
            .iter()
            .filter(|value| value.show.get() )
            .map(|value| value.clone())
            .collect::<Vec<Notification>>()
}

  #[component(transparent)]
  pub fn NotificationsPage(notification_read_signal: ReadSignal<Notifications>,
                           notification_write_signal: WriteSignal<Notifications>)  -> impl IntoView {
    let notifications = create_local_resource(notification_read_signal, filter_notifications);

    create_effect(move |_| {
        leptos::set_interval_with_handle(move || {
            notification_write_signal.update(|n: &mut Notifications| {
                let notifications = n.notifications
                            .iter()
                            .filter(|value| value.show.get())
                            .map(|value| Notification::new(value.message.clone()))
                            .collect::<Vec<Notification>>();
                let new_notify = Notifications::new(notifications);

                *n = new_notify;
            })
        }, Duration::from_secs(7))});

    view! {
          <Suspense fallback=|| "">
             {notifications.get().unwrap_or_default().iter().map(|value: &Notification| {
                 let cloned_notify = value.clone();
                 view!{
                     <div class="flex flex-row z-100 absolute left-0 top-0">
                         <AnimatedShow
                            when=cloned_notify.show.clone()
                            show_class="fadeIn500"
                            hide_class="fadeOut500"
                            hide_delay=core::time::Duration::from_millis(300)
                        >
                            {cloned_notify.message.clone().to_html()}
                        </AnimatedShow>
                     </div>
                 }
             }).collect::<Vec<_>>()}
          </Suspense>
    }
}
