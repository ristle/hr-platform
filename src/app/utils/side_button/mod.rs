use leptos::html::Div;
use leptos::*;

#[derive(Clone)]
pub enum StartUpButtonStatus {
    Active,
    None
}

/// TODO: Define closure as type
#[derive(Clone)]
pub struct SideButton {
    pub write_signal: WriteSignal<String>,
    pub read_signal: ReadSignal<String>,
    pub name: &'static str,
    pub startup_status: StartUpButtonStatus,
    pub tab_to_set: fn() -> HtmlElement<Div>,
}

impl SideButton {
    pub fn new(button_name: &'static str, tab_to_set: fn() -> HtmlElement<Div>, startup_status: StartUpButtonStatus) -> SideButton {
        let (get_signal, set_signal) = create_signal("tab-button normal-text ".to_string());

        SideButton {
            write_signal: set_signal,
            read_signal: get_signal,
            name: button_name,
            startup_status,
            tab_to_set,
        }
    }
}
