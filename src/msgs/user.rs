use chrono;
#[cfg(feature = "ssr")]
use diesel::prelude::*;
use serde::{Deserialize, Serialize};

cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {

        #[derive(Insertable, Clone, Debug)]
        #[diesel(table_name = crate::db::schema::platform_user)]
        pub struct NewPlatformUser<'a> {
            pub name: &'a str,
            pub surname: &'a str,
            pub email: &'a str,
            pub role: &'a str,
            pub password:  &'a str,
            pub photo_url: Option<&'a str>,
            pub verified: bool,
            pub created_at: chrono::NaiveDate,
            pub updated_at: chrono::NaiveDate,
        }
    }
}

#[cfg_attr(
    feature = "ssr",
    derive(Queryable, Selectable, Default, Debug, Clone, Serialize, Deserialize)
)]
#[cfg_attr(feature = "ssr", diesel(table_name = crate::db::schema::platform_user))]
#[cfg_attr(feature = "ssr", diesel(check_for_backend(diesel::pg::Pg)))]
#[cfg_attr(
    feature = "hydrate",
    derive(Default, Clone, Debug, Serialize, Deserialize)
)]
pub struct PlatformUser {
    pub id: i32,
    pub name: String,
    pub surname: String,
    pub email: String,
    pub role: String,
    pub password: String,
    pub photo_url: Option<String>,
    pub verified: bool,
    pub created_at: chrono::NaiveDate,
    pub updated_at: chrono::NaiveDate,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct NewPlatformUserJson {
    pub name: String,
    pub surname: String,
    pub email: String,
    pub role: String,
    pub password: String,
}
