cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::resume)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct Resume {
            pub id: i32,
            pub resume_name: String,
            pub user_id: i32,
            pub photo_url: Option<String>,
            pub job_name: String,
            pub work_type: String,
            pub work_experience: f64,
            pub salary_expectations: Option<i32>,
            pub location: Option<String>,
            pub skill_id: i32,
            pub languages_id: i32,
            pub description: String,
            pub work_id: i32,
            pub education_id: i32,
            pub lookup_vacancies: i32,
        }

        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::resume)]
        pub struct NewResume {
            pub resume_name: &'static str,
            pub user_id: i32,
            pub photo_url: Option<&'static str>,
            pub job_name: &'static str,
            pub work_type: &'static str,
            pub work_experience: f64,
            pub salary_expectations: Option<i32>,
            pub location: Option<&'static str>,
            pub skill_id: i32,
            pub languages_id: i32,
            pub description: &'static str,
            pub work_id: i32,
            pub education_id: i32,
            pub lookup_vacancies: i32,
        }

    }
}
