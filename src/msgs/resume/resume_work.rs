cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;


        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::resume_work)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct ResumeWork {
            pub id: i32,
            pub resume_id: i32,
            pub language_name: String,
            pub details_info: Option<String>,
            pub experience_time: String,
        }
        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::resume_work)]

        pub struct NewResumeWork {
            pub resume_id: i32,
            pub language_name: &'static str,
            pub details_info: Option<&'static str>,
            pub experience_time: &'static str,
        }
    }
}
