cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::resume_language)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
            pub struct ResumeLanguage {
                pub id: i32,
                pub resume_id: i32,
                pub language_name: String,
                pub experience_time: String,
            }

            #[derive(Insertable)]
            #[diesel(table_name = crate::db::schema::resume_language)]
            pub struct NewResumeLanguage {
                pub resume_id: i32,
                pub language_name: &'static str,
                pub experience_time: &'static str,
            }
    }
}
