cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::vacancy)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct Vacancy {
            pub id: i32,
            pub job_name: String,
            pub company_name: String,
            pub work_experience: f64,
            pub work_type: String,
            pub feature_id: i32,
            pub new_responses: i32,
            pub unread_responses: i32,
            pub skills: i32,
            pub details: String,
            pub vacancy_stages: i32,
            pub people_in_work_id: i32,
            pub related_people_id: i32,
        }

        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::vacancy)]
        pub struct NewVacancy {
            pub job_name: &'static str,
            pub company_name: &'static str,
            pub work_experience: f64,
            pub work_type: &'static str,
            pub feature_id: i32,
            pub new_responses: i32,
            pub unread_responses: i32,
            pub skills: i32,
            pub details: &'static str,
            pub vacancy_stages: i32,
            pub people_in_work_id: i32,
            pub related_people_id: i32,
        }
    }
}
