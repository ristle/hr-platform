cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::vacancy_response)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct VacancyResponse {
            pub id: i32,
            pub user_id: i32,
            pub vacancy_id: i32,
            pub status: String,
            pub vacancy_stage_id: i32,
            pub hr_user_id: i32,
            pub is_read: bool,
        }

        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::vacancy_response)]
        pub struct NewVacancyResponse {
            pub user_id: i32,
            pub vacancy_id: i32,
            pub status: &'static str,
            pub vacancy_stage_id: i32,
            pub hr_user_id: i32,
            pub is_read: bool,
        }
    }
}
