cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::vacancy_skill)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct VacancySkill {
            pub id: i32,
            pub user_id: i32,
            pub name: String,
            pub experience_time: f64,
        }

        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::vacancy_skill)]
        pub struct NewVacancySkill {
            pub user_id: i32,
            pub name: &'static str,
            pub experience_time: f64,
        }

    }
}
