cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::vacancy_people_in_work)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct VacancyPeopleInWork {
            pub id: i32,
            pub user_id: i32,
        }

        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::vacancy_people_in_work)]
        pub struct NewVacancyPeopleInWork {
            pub user_id: i32,
        }
    }
}
