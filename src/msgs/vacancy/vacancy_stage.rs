cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::vacancy_stage)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct VacancyStage {
            pub id: i32,
            pub vacancy_stage_id: i32,
            pub name: String,
            pub is_active: bool,
        }

        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::vacancy_stage)]
        pub struct NewVacancyStage {
            pub vacancy_stage_id: i32,
            pub name: &'static str,
            pub is_active: bool,
        }
    }
}
