cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::vacancy_people_related)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct VacancyPeopleRelated {
            pub id: i32,
            pub user_id: i32,
        }

        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::vacancy_people_related)]
        pub struct NewVacancyPeopleRelated {
            pub user_id: i32,
        }
    }
}
