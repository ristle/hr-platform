cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::vacancy_stages)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct VacancyStages {
            pub id: i32,
            pub name: String,
        }

        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::vacancy_stages)]
        pub struct NewVacancyStages {
            pub name: &'static str,
        }
    }
}
