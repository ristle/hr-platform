cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use chrono;
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::chat_user_group)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct ChatUserGroup {
            pub id: i32,
            pub user_id: i32,
            pub group_id: i32,
            pub created_date: chrono::NaiveDate,
            pub is_active: bool,
        }

        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::chat_user_group)]
        pub struct NewChatUserGroup {
            pub user_id: i32,
            pub group_id: i32,
            pub created_date: chrono::NaiveDate,
            pub is_active: bool,
        }

    }
}
