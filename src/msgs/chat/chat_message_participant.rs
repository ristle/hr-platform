cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::chat_message_participant)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct ChatMessageParticipant {
            pub id: i32,
            pub recipient_id: i32,
            pub recipient_group_id: i32,
            pub message_id: i32,
            pub is_read: bool,
        }
        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::chat_message_participant)]
        pub struct NewChatMessageParticipant {
            pub recipient_id: i32,
            pub recipient_group_id: i32,
            pub message_id: i32,
            pub is_read: bool,
        }
    }
}
