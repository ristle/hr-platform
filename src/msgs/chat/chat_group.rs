cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use chrono;
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::chat_group)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct ChatGroup {
            pub id: i32,
            pub group_name: String,
            pub created_time: chrono::NaiveDate,
            pub is_active: bool,
        }
        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::chat_group)]
        pub struct NewChatGroup {
            pub group_name: &'static str,
            pub created_time: chrono::NaiveDate,
            pub is_active: bool,
        }

    }
}
