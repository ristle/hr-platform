cfg_if::cfg_if! {
    if #[cfg(feature = "ssr")] {
        use chrono;
        use diesel::prelude::*;

        #[derive(Queryable, Selectable)]
        #[diesel(table_name = crate::db::schema::chat_message)]
        #[diesel(check_for_backend(diesel::pg::Pg))]
        pub struct ChatMessage {
            pub id: i32,
            pub subject: String,
            pub creator_id: i32,
            pub message_body: String,
            pub create_date: chrono::NaiveDate,
        }
        #[derive(Insertable)]
        #[diesel(table_name = crate::db::schema::chat_message)]
        pub struct NewChatMessage {
            pub subject: &'static str,
            pub creator_id: i32,
            pub message_body: &'static str,
            pub create_date: chrono::NaiveDate,
        }

    }
}
