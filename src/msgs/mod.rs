#[cfg(feature = "ssr")]
use diesel::PgConnection;

pub mod chat;
pub mod resume;
pub mod user;
pub mod vacancy;


#[cfg(feature = "ssr")]
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Config {
    pub jwt_secret: String,
    pub jwt_expires_in: String,
    pub jwt_maxage: i32,
}

#[cfg(feature = "ssr")]
impl Config {
    pub fn init() -> Config {
        let jwt_secret = std::env::var("JWT_SECRET").expect("JWT_SECRET must be set");
        let jwt_expires_in = std::env::var("JWT_EXPIRED_IN").expect("JWT_EXPIRED_IN must be set");
        let jwt_maxage = std::env::var("JWT_MAXAGE").expect("JWT_MAXAGE must be set");
        Config {
            jwt_secret,
            jwt_expires_in,
            jwt_maxage: jwt_maxage.parse::<i32>().unwrap(),
        }
    }
}

#[cfg(feature = "ssr")]
pub struct ServerData {
    pub connection: PgConnection,
    pub env: Config,
    pub user_data: std::collections::HashMap<&'static str, String>,
}
