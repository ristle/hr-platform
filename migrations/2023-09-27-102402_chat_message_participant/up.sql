-- Your SQL goes here
create table chat_message_participant (
    id serial primary key,
    recipient_id int not null,
    recipient_group_id int not null,
    message_id int not null,
    is_read boolean not null
);