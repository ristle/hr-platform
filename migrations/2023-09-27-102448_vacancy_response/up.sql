-- Your SQL goes here
create table vacancy_response (
    id serial primary key,
    user_id int not null,
    vacancy_id int not null,
    status varchar not null,
    vacancy_stage_id int not null,
    hr_user_id int not null,
    is_read boolean not null default false
);