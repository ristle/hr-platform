-- Your SQL goes here
create table chat_message (
    id serial primary key,
    subject varchar not null,
    creator_id int not null,
    message_body varchar not null,
    create_date date not null
);