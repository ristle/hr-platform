-- Your SQL goes here
create table vacancy_skill (
    id serial primary key,
    user_id int not null,
    name varchar not null,
    experience_time float not null
);