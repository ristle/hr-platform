-- Your SQL goes here
-- This file should undo anything in `up.sql`
create table resume (
    id serial primary key,
    resume_name varchar not null,
    user_id int not null,
    photo_url varchar null,
    job_name varchar not null,
    work_type varchar not null,
    work_experience float not null,
    salary_expectations int null,
    location varchar null,
    skill_id int not null,
    languages_id int not null,
    description varchar not null,
    work_id int not null,
    education_id int not null,
    lookup_vacancies int not null
);