-- Your SQL goes here
create table resume_work (
     id serial primary key,
     resume_id int not null,
     language_name varchar not null,
     details_info varchar null,
     experience_time varchar not null
);