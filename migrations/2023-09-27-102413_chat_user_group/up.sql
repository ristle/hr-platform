-- Your SQL goes here
create table chat_user_group (
    id serial primary key,
    user_id int not null,
    group_id int not null,
    created_date Date not null,
    is_active boolean not null
);