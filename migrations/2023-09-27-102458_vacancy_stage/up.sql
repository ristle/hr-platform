-- Your SQL goes here
create table vacancy_stage (
    id serial primary key,
    vacancy_stage_id int not null,
    name varchar not null,
    is_active boolean not null
);