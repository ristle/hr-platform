-- Your SQL goes here
create table resume_skill (
    id serial primary key,
    resume_id int not null,
    skill_name varchar not null,
    experience_time varchar not null
);