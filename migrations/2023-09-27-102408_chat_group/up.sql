-- Your SQL goes here
create table chat_group (
    id serial primary key,
    group_name varchar not null,
    created_time date not null,
    is_active boolean not null default false
);