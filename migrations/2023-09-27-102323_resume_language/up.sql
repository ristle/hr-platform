-- Your SQL goes here
create table resume_language (
      id serial primary key,
      resume_id int not null,
      language_name varchar not null,
      experience_time varchar not null
);