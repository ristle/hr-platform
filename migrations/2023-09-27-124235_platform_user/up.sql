-- Your SQL goes here
create table platform_user (
    id serial primary key,
    name varchar not null,
    surname varchar not null,
    email varchar not null,
    password varchar not null,
    role varchar not null,
    photo_url varchar null,
    verified boolean not null default false,
    created_at date not null,
    updated_at date not null
);