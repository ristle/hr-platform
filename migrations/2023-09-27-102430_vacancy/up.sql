create table vacancy (
    id serial primary key,
    job_name varchar not null,
    company_name varchar not null,
    work_experience float not null,
    work_type varchar not null,
    feature_id int not null,
    new_responses int not null,
    unread_responses int not null,
    skills int not null,
    details varchar not null,
    vacancy_stages int not null,
    people_in_work_id int not null,
    related_people_id int not null
);