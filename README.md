# **Super Duper HR Platform**

## SetUp

```bash
cargo install diesel_cli  --features postgres
cargo install cargo-leptos

diesel setup
diesel migration run
```